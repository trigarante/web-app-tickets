import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { IncidencesModel } from '../../Models/Incidences/Incidences@Model';
import { IncidencesDetailsModel } from '../../Models/Incidences/IncidencesDetails@Model';

@Injectable({ providedIn: 'root' })
export class IncidensesService {

    constructor(
        private http: HttpClient
    ) { }

    getInstancesList(): Observable<IncidencesModel[]> {
        return this.http.get<IncidencesModel[]>(`${environment.baseURL}/incidences`);
    }

    getIncidencesDetailsListById(incidenceID: number): Observable<IncidencesDetailsModel[]> {
        return this.http.get<IncidencesDetailsModel[]>(`${environment.baseURL}/incidences-details/${incidenceID}`);
    }

}