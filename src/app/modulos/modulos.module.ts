import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ModulosRoutingModule } from './modulos-routing.module';
import { ModulosComponent } from './modulos.component';
import { DashboardComponent } from './dashboard/dashboard/dashboard.component';
import { ThemeModule } from '../@theme/@theme.module';
import { MatIconModule } from '@angular/material/icon';
import { FormsModule } from '@angular/forms';
import { ModalComponent } from './my-tickets/modal-component/modal/modal.component';
import { MatButtonModule } from '@angular/material/button';
import { MatExpansionModule } from '@angular/material/expansion';
import { MessagesService } from '../@core/Services/Messages/messages.service';
import {MatMenuModule} from '@angular/material/menu';
import {SharedModule} from '../shared/shared.module';



@NgModule({
  declarations: [
    DashboardComponent,
    ModulosComponent,
    ModalComponent,
  ],
  imports: [
    CommonModule,
    ModulosRoutingModule,
    ThemeModule,
    MatIconModule,
    MatButtonModule,
    MatExpansionModule,
    FormsModule,
    MatMenuModule,
    SharedModule
  ],
  providers: [
    MessagesService
  ]
})
export class ModulosModule { }
