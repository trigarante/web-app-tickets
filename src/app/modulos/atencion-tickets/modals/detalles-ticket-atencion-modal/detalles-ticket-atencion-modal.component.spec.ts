import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DetallesTicketAtencionModalComponent } from './detalles-ticket-atencion-modal.component';

describe('DetallesTicketAtencionModalComponent', () => {
  let component: DetallesTicketAtencionModalComponent;
  let fixture: ComponentFixture<DetallesTicketAtencionModalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DetallesTicketAtencionModalComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DetallesTicketAtencionModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
