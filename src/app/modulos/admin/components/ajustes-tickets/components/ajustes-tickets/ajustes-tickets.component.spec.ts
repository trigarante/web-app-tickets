import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AjustesTicketsComponent } from './ajustes-tickets.component';

describe('AjustesTicketsComponent', () => {
  let component: AjustesTicketsComponent;
  let fixture: ComponentFixture<AjustesTicketsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AjustesTicketsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AjustesTicketsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
