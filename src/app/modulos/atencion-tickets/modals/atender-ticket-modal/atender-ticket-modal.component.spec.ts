import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AtenderTicketModalComponent } from './atender-ticket-modal.component';

describe('AtenderTicketModalComponent', () => {
  let component: AtenderTicketModalComponent;
  let fixture: ComponentFixture<AtenderTicketModalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AtenderTicketModalComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AtenderTicketModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
