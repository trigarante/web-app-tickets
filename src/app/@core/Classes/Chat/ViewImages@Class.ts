import { MatDialog } from "@angular/material/dialog";
import { ImagesCarrouselChatComponent } from "src/app/shared/images-carrousel-chat/images-carrousel-chat.component";

export class ViewImagesClass {

    constructor(
        private dialog: MatDialog
    ) { }

    viewImages(imagesArray: object[], selectedImage: string) {
        this.dialog.open(ImagesCarrouselChatComponent, {
            data: {
                imagesArray: imagesArray,
                selectedImage: selectedImage
            },
            width: '1200px',
            height: '600px',
            disableClose: false
        });
    }

}