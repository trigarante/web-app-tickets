import { Component, Inject, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { AtentionAreasModel } from 'src/app/@core/Models/AtentionAreas/AtentionAreas@Model';
import { AtentionAreasService } from 'src/app/@core/Services/AtentionAreas/atentionareas.service';
import { EmployeesService } from 'src/app/@core/Services/Employess/employees.service';
import { TicketsService } from 'src/app/@core/Services/Tikcets/tickets.service';

import Swal from 'sweetalert2';

@Component({
    selector: 'app-ajustes-tickets-modal',
    templateUrl: 'ajustes-tickets-modal.component.html',
    styleUrls: ['ajustes-tickets-modal.component.scss']
})

export class AjustesTicketsModalComponent implements OnInit {

    ticketID: number;
    atencionAreaID: number;
    attendingEmployeesTickets = [];
    tabIndex: number;
    formGroup: FormGroup;
    atentionAreasArray: AtentionAreasModel[];

    hasAchange: boolean = false;

    constructor(
        private dialogRef: MatDialogRef<AjustesTicketsModalComponent>,
        @Inject(MAT_DIALOG_DATA) public dataExternalDialog: any,
        private employeesService: EmployeesService,
        private atentionAreasService: AtentionAreasService,
        private ticketsService: TicketsService,
        private formBuilder: FormBuilder
    ) {
        this.startFormBuilder();
        this.ticketID = this.dataExternalDialog.ticketID;
        this.atencionAreaID = this.dataExternalDialog.idAreaAtencion;
        this.tabIndex = this.dataExternalDialog.tabIndex;
        this.loadAttendingEmployeeByTicketID();
        // this.loadAttendingEmployees();
        this.loadAttentionArea();
    }

    ngOnInit() { }

    startFormBuilder() {
        this.formGroup = this.formBuilder.group({
            employeeAttending: [null, [Validators.required]],
            atentionArea: [null, [Validators.required]],
            checkboxReabrirTicket: [false]
        });
    }

    closeModal() {
        this.dialogRef.close({ success: false });
    }

    loadAttendingEmployeeByTicketID() {
        let dataEmployeeAttending = [];
        this.employeesService.getEmployeeAttendingTicketById(this.ticketID).subscribe({
            next: (result) => {
                dataEmployeeAttending = Object.values(result['employeeTicket'][0]);
            },
            error: (error) => {
                console.error('Error al consultar el empleado que atiende este ticket => ', error);
            },
            complete: () => {
                this.employeeAttendingField.setValue(dataEmployeeAttending[0].idEmpleado);
            }
        });
    }

    loadAttendingEmployees() {
        let attendingEmployeesResponse = [];
        this.employeesService.getEmployeesByAtentionAreaID(this.atencionAreaID).subscribe({
            next: (response) => { attendingEmployeesResponse = Object.values(response['atentionAreaEmployees'][0]); },
            error: (error) => { console.error('Error al consumir el servicio => ', error); },
            complete: () => { this.attendingEmployeesTickets = attendingEmployeesResponse; }
        });
    }

    loadAttentionArea() {
        let atentionAreas: AtentionAreasModel[];
        this.atentionAreasService.getAtentionAreasList().subscribe({
            next: (data) => {
                atentionAreas = data;
            },
            error: (error) => {
                console.error('A ocurrido un error al consultar las Áreas de Atención => ', error);
            },
            complete: () => {
                this.atentionAreasArray = atentionAreas;
                this.atentionAreaField.setValue(this.atencionAreaID);
                this.loadAttendingEmployees();
            }
        });
    }

    changeArea() {
        this.atencionAreaID = this.atentionAreaField.value;
        this.employeeAttendingField.setValue(null);
        this.loadAttendingEmployees();
    }

    changeAsesor() {
        this.hasAchange = true;
    }

    saveChanges() {
        if (this.hasAchange === true) {
            if (this.formGroup.valid) {
                let dataResponse;
                const dataToUpdate = { areaID: this.atentionAreaField.value, empleadoID: this.employeeAttendingField.value };
                this.ticketsService.putAtentionInfoByTicketID(this.ticketID, dataToUpdate).subscribe({
                    next: (response) => {
                        dataResponse = response;
                    },
                    error: (error) => {
                        Swal.fire({
                            title: 'Error al guardar cambios',
                            text: 'Ocurrió un error al reasignar el ticket a otro asesor',
                            icon: 'error'
                        });
                        console.error('Error al actualizar la informacion en la base de datos => ', error);
                    },
                    complete: () => {
                        Swal.fire({
                            title: 'Cambios guardados',
                            text: 'El ticket ha sido reasignado exitosamente',
                            icon: 'success'
                        });
                        this.dialogRef.close({ success: true });
                    }
                });
            }
        } else {
            this.dialogRef.close({ success: false });
        }
    }

    // Propiedades get para acceder a los valores del formGroup
    get employeeAttendingField(): AbstractControl {
        return this.formGroup.get('employeeAttending');
    }

    get atentionAreaField(): AbstractControl {
        return this.formGroup.get('atentionArea');
    }

    get checkboxReabrirTicketField(): AbstractControl {
        return this.formGroup.get('checkboxReabrirTicket');
    }

}