import { AfterViewInit, Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import Swal from 'sweetalert2';
import { Router } from '@angular/router';
import { MatDrawer, MatSidenav } from '@angular/material/sidenav';
import { UsuarioService } from '../../@core/Services/usuario.service';
import { EmployeesService } from 'src/app/@core/Services/Employess/employees.service';
import { MenusModel } from 'src/app/@core/Models/Menus/Menus@Model';
import { io } from 'socket.io-client';

import iziToast from 'izitoast';
import { MatDialog } from '@angular/material/dialog';
import { MessagesComponent } from '../../shared/messages/messages.component';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.scss']
})
export class ToolbarComponent implements OnInit, AfterViewInit {
  @ViewChild('sidenav') sidenav: MatSidenav;

  // Atributos para los sonidos de las notificaciones
  @ViewChild('newMessageNotify') messageNotify: ElementRef;
  @ViewChild('newChangeStatusNotify') changeStatusNotify: ElementRef;

  menuItemsController: MenusModel[];

  // Atributo donde se almacenará el socket
  socket: any;
  profile: any;
  public auth2: any;

  // Atributo para almacenar el tamaño del array de notificaciones
  notification: number;
  // Arreglo para almacenar las notificaciones
  static notifications = [];

  constructor(
    private router: Router,
    private usuarioService: UsuarioService,
    private employeesService: EmployeesService,
    private dialog: MatDialog
  ) {
    this.profile = JSON.parse(localStorage.getItem('profile'));
    setTimeout(() => {
      this.getEmployeeMenus();
    }, 1000);
    this.setupSocketConnection();
    if (!localStorage.getItem('chatOpen')) {
      localStorage.setItem('chatOpen', 'false');
    }
    if (window.performance.navigation.type === 1) {
      localStorage.setItem('chatOpen', 'false');
    }

  }

  ngAfterViewInit(): void {
    this.messageNotify.nativeElement.muted = true;
    this.messageNotify.nativeElement.play();
    this.changeStatusNotify.nativeElement.muted = true;
    this.changeStatusNotify.nativeElement.play();
  }

  ngOnInit(): void { }

  setupSocketConnection() {
    this.socket = io(environment.SOCKET_ENDPOINT, {
      query: {
        token: ''
      }
    }), {
      // Se implementaron los permisos para cors
      withCredentials: true,
      extraHeaders: {
        "my-custom-header": "abcd"
      }
    };

    this.socket.on('notificacion', (data) => {
      const chatOpen = localStorage.getItem('chatOpen');
      if (chatOpen === 'false') {
        if (data) {
          const reciever = data.reciever;
          const ticketID = data.ticketid;
          const photo = data.photo;
          const ticketStatus = data.ticketstatus;
          const sender = data.sender;
          const employeeInfo = JSON.parse(localStorage.getItem('employeeInfo'));
          let message = '';
          if (data.message !== null) {
            message = data.message.length > 45 ? data.message.substring(0, 45) + ' ...' : data.message;
          }
          if (parseInt(reciever) === parseInt(employeeInfo.idEmpleado)) {
            // const imgProfile = 'https://lh3.googleusercontent.com/a-/AOh14GiLt9TcXhPCaDTbLzFuuc1Htk9CgfJePO4U8Ieh=s96-c';
            switch (data.typenotification) {
              case 'newmessage':
                iziToast.show({
                  title: 'Nuevo Mensaje en el Ticket #' + ticketID,
                  titleSize: '16',
                  message: message,
                  messageSize: '14',
                  layout: 2,
                  position: 'bottomRight',
                  image: photo,
                  imageWidth: 70,
                  backgroundColor: '#70f7b8',
                  progressBarColor: '#575c70',
                  transitionIn: 'bounceInDown',
                  closeOnClick: true,
                  maxWidth: 450,
                  close: false,
                  timeout: 5000,
                  onClosing: (instance, toast, closedBy) => {
                    if (closedBy === 'toast') {
                      this.openDialogMessages(ticketID, ticketStatus, sender);
                    } else if (closedBy === 'timeout') {
                      ToolbarComponent.notifications.push({ des: 'descripcion' });
                    }

                  }
                });
                this.messageNotify.nativeElement.muted = false;
                this.messageNotify.nativeElement.play();
                break;
              case 'changestatus':
                iziToast.info({
                  title: 'Cambio de estado del Ticket #' + ticketID,
                  titleSize: '16',
                  message: message,
                  messageSize: '14',
                  layout: 2,
                  position: 'bottomRight',
                  progressBarColor: '#575c70',
                  transitionIn: 'bounceInDown',
                  maxWidth: 450,
                  timeout: 5000,
                  onClosing: (instance, toast, closedBy) => { }
                });
                this.changeStatusNotify.nativeElement.muted = false;
                this.changeStatusNotify.nativeElement.play();
                break;
            }
          }
        }
      }
    });
  }

  private openDialogMessages(ticketID: number, ticketStatus: number, receiver: number) {
    const chatOpen = localStorage.getItem('chatOpen');
    if (chatOpen === 'false') {
      localStorage.setItem('chatOpen', 'true');
      this.dialog.open(MessagesComponent, {
        panelClass: 'messengerStylesModal',
        data: {
          ticketId: ticketID,
          ticketStatus: ticketStatus,
          receiver: receiver
        },
        disableClose: true
      });
    }

  }

  private getEmployeeMenus() {
    let menusEmployee: MenusModel[];
    this.employeesService.getEmployeeMenusById().subscribe({
      next: (response) => {
        menusEmployee = response['menus'][0];
      },
      error: (error) => {
        console.error('Error al consultar los menus => ', error);
      },
      complete: () => {
        this.menuItemsController = Object.values(menusEmployee);
      }
    });
  }

  close() {
    this.sidenav.close();
  }

  redirectDashboard(): any {
    this.router.navigate(['modulos/dashboard']);
  }

  cerrarSesion(): any {
    Swal.fire({
      title: '¿Desea Cerrar Sesión?',
      icon: 'question',
      showConfirmButton: true,
      showCancelButton: true,
      cancelButtonText: 'NO',
      confirmButtonText: 'SI',
      cancelButtonColor: 'red',
      confirmButtonColor: 'green',
    }).then(resp => {
      if (resp.value) {
        this.updateStatusSession();
        this.usuarioService.logout();
        this.sidenav.close();
        Swal.fire({
          title: 'Se ha Cerrado la Sesión',
          icon: 'success',
        });
      }
    });
  }

  get Notification(): number {
    return ToolbarComponent.notifications.length;
  }

  updateStatusSession() {
    const tipoUsuario = JSON.parse(localStorage.getItem('employeeInfo')).fkIdTipoUsuario;
    const email = JSON.parse(localStorage.getItem('profile')).profileemail;
    if (parseInt(tipoUsuario) === 3) {
      this.usuarioService.updateSessionActive(email, false).subscribe({
        next: (response) => { },
        error: (error) => { console.error('Error logout update status => ', error); },
        complete: () => { }
      });
    }
  }

}
