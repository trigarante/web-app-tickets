import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PermisosComponent } from './components/permisos/permisos.component';
import { AdminRoutingModule } from './admin-routing.module';
import { ThemeModule } from 'src/app/@theme/@theme.module';
import { MatFormFieldModule } from '@angular/material/form-field';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatInputModule } from '@angular/material/input';
import { MatSortModule } from '@angular/material/sort';
import { MatTableModule } from '@angular/material/table';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatSelectModule } from '@angular/material/select';
import { MatButtonModule } from '@angular/material/button';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatIconModule } from '@angular/material/icon';
import { MatDialogModule } from '@angular/material/dialog';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatTabsModule } from '@angular/material/tabs';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { SharedModule } from 'src/app/shared/shared.module';
import { AjustesTicketsComponent } from './components/ajustes-tickets/components/ajustes-tickets/ajustes-tickets.component';
import { ModalDetailsComponent } from './components/ajustes-tickets/components/modal-details/modal-details.component';
import { AssignTicketsModalComponent } from './components/ajustes-tickets/components/assign-tickets-modal/assign-tickets-modal.component';
import { AjustesTicketsModalComponent } from './components/ajustes-tickets/components/ajustes-tickets-modal/ajustes-tickets-modal.component';
import { AjustesPerfilesComponent } from './components/ajustes-perfiles/components/ajustes-perfiles/ajustes-perfiles.component';
import { ManageProfilesComponent } from './components/ajustes-perfiles/components/manage-profiles/manage-profiles.component';
import { TableTicktesNotAssignedComponent } from './components/ajustes-tickets/components/table-tickets-not-assigned/table-tickets-not-assigned.component';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';

@NgModule({
  declarations: [
    PermisosComponent,
    AjustesTicketsComponent,
    ModalDetailsComponent,
    AssignTicketsModalComponent,
    AjustesTicketsModalComponent,
    AjustesPerfilesComponent,
    ManageProfilesComponent,
    TableTicktesNotAssignedComponent
  ],
  imports: [
    CommonModule,
    AdminRoutingModule,
    ThemeModule,
    MatFormFieldModule,
    MatTableModule,
    MatPaginatorModule,
    MatInputModule,
    ReactiveFormsModule,
    FormsModule,
    MatSelectModule,
    MatButtonModule,
    MatExpansionModule,
    MatIconModule,
    MatDialogModule,
    MatSortModule,
    MatTooltipModule,
    MatTabsModule,
    SharedModule,
    MatCheckboxModule,
    MatProgressSpinnerModule
  ]
})
export class AdminModule { }
