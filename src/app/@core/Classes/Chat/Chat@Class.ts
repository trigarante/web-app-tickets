import { io } from 'socket.io-client';
import { environment } from 'src/environments/environment';
import { MessagesModel } from '../../Models/Messages/Messages@Model';
import { MessagesService } from '../../Services/Messages/messages.service';

export class ChatClass {

    // Atributo donde se almacenará el socket
    socket: any;
    // Atributo para almacenar el mensaje que se escribirá desde la caja de texto
    message: string = '';
    // Array donde se almacenaranlos mensajes que se envian y se reciben
    messagesArray = [];
    // Almacena los datos del perfil de google de la perona que inició sesión
    profile: any;
    // Objeto para instanciar la clase Date
    currentDate: Date;
    // Almacena el id del Ticket donde se hará el chat
    ticketID: number;
    // Almacena el id del empleado que atiende el ticket
    receiver: number;
    // Atributo para almacenar el tamaño del array de notificaciones
    notification: Number;
    // Arreglo para almacenar las notificaciones
    notifications = [];
    // Atributo para almacenar el estado del ticket
    ticketStatus: number;
    // Atributo para almacenar los datos del empleado que estan en el storage
    employeeInfo: any;
    // Atributo para almacenar la url de la imagen
    imageurl: string;

    constructor(
        ticketID: number,
        private messagesService: MessagesService,
        receiver: number,
        ticketStatus: number,
        private isFirstMessage: boolean
    ) {
        this.ticketID = ticketID;
        this.receiver = receiver;
        this.profile = JSON.parse(localStorage.getItem('profile'));
        this.employeeInfo = JSON.parse(localStorage.getItem('employeeInfo'));
        this.ticketStatus = ticketStatus;
        if (!this.isFirstMessage) {
            this.getMessagesOnlyById(this.ticketID);
        }
    }

    // Método que se ejecuta cuando se inicia el componente del front
    setupSocketConnection() {
        // Se hace la conexión con el api
        this.socket = io(environment.SOCKET_ENDPOINT, {
            // Se envía un parametro extra el cual es recibido en el backend cuando se realiza la conexión
            query: {
                token: `ticket_${this.ticketID}`
            }
        }), {
            // Se implementaron los permisos para cors
            withCredentials: true,
            extraHeaders: {
                "my-custom-header": "abcd"
            }
        };
        // Se ejecuta cuando el socket se encuentra sobre el broadcast en ambos lados front y back
        this.socket.on('broadcast', (data) => {
            if (data) {
                const objDiv = document.querySelector('#principal-container > #container-msg-list');
                this.messagesArray.push({
                    message: data.message,
                    profileid: data.profileId,
                    profilename: data.profileName,
                    datesend: data.dateSend,
                    timesend: data.timeSend,
                    image: data.image
                });
                // Funcion para que el scroll siempre se vaya hasta el final cuando se envíe o reciba un nuevo mensaje
                setTimeout(() => {
                    objDiv.scrollTop = (objDiv.scrollHeight);
                }, 100);
            }
        });

        this.message = '';
        this.imageurl = '';
    }

    // Método para enviar el mensaje, agregarlo al array también
    SendMessage() {
        if (this.ticketStatus === 2) {
            const objDiv = document.querySelector('#principal-container > #container-msg-list');
            const ticketId = this.ticketID;
            const dateNow = this.getCurrentDate();
            const timeNow = this.getCurrentTime();
            // Se envía el mensaje al back y con sockets se hace llegar al destinatario
            this.socket.emit('dataSocket', {
                ticketid: ticketId,
                message: this.message,
                profileId: this.profile['profileid'],
                profileName: this.profile['profilename'],
                dateSend: dateNow,
                timeSend: timeNow,
                image: this.imageurl
            });
            // Se agrega al array de mensajes
            this.messagesArray.push({
                message: this.message,
                profileid: this.profile['profileid'],
                profilename: this.profile['profilename'],
                datesend: dateNow,
                timesend: timeNow,
                image: this.imageurl
            });
            this.message = '';
            this.imageurl = '';

            // Funcion para que el scroll siempre se vaya hasta el final cuando se envíe o reciba un nuevo mensaje
            setTimeout(() => {
                objDiv.scrollTop = (objDiv.scrollHeight);
            }, 100);

            /* Funciona cuando un chat envía un mensaje y si el otro no lo tiene abierto, este le llegue ya que se
            guardará en la base de datos y así cuando el otro chat se abra, se cargará de forma inmediata el mensaje */
            if (this.messagesArray.length > 0) {
                this.saveMessagesSent();
            }
        }
    }

    keyPressEnter(e) {
        if (e.keyCode === 13) {
            if (this.message.trim() === '' && this.imageurl.trim() === '') {
                return;
            }
            this.SendMessage();
        }
    }

    // Cierra la conexión del socket
    closeSocketConnection() {
        this.saveMessagesSent();
        this.socket.disconnect();
    }

    // Método para definir si hará un registro nuevo de los mensajes o se actualizará el existente
    private saveMessagesSent() {
        // Atributo para validar que el chat ya es existente en la base de datos
        let existChatOnDataBase: boolean = false;
        this.messagesService.getMessagesByIdTicket(this.ticketID).subscribe({
            next: (data) => { data['mensajes'].length > 0 ? existChatOnDataBase = true : existChatOnDataBase = false; },
            error: (error) => { console.error('Error al verificar los mensajes en el chat => ', error); },
            complete: () => {
                if (existChatOnDataBase) {
                    this.putMessagesChat(this.messagesArray);
                } else if (existChatOnDataBase === false && this.messagesArray.length > 0) {
                    this.postMessagesChat(this.messagesArray);
                }
            }
        });
    }

    // Funciones para obtener los datos de fecha y hora
    public getCurrentDate(): string {
        this.currentDate = new Date();
        const dayNow = `${this.currentDate.getDate() < 10 ? '0' + this.currentDate.getDate() : this.currentDate.getDate()}`;
        const monthNow = `${(this.currentDate.getMonth() + 1) < 10 ? '0' + (this.currentDate.getMonth() + 1) : this.currentDate.getMonth() + 1}`;
        const dateNow = `${dayNow}/${monthNow}/${this.currentDate.getFullYear()}`;
        return dateNow;
    }

    public getCurrentTime(): string {
        this.currentDate = new Date();
        const currentHour = `${this.currentDate.getHours() < 10 ? '0' + this.currentDate.getHours() : this.currentDate.getHours()}`;
        const currentMinutes = `${this.currentDate.getMinutes() < 10 ? '0' + (this.currentDate.getMinutes()) : this.currentDate.getMinutes()}`;
        const timeNow = `${currentHour}:${currentMinutes}`;
        return timeNow;
    }

    // Metodo para consultar los mensajes del ticket cual ID está siendo recibido como parámetros
    private getMessagesOnlyById(ticketID: number) {
        let onlyMessages: JSON[] = null;
        this.messagesService.getMessagesByIdTicket(ticketID).subscribe({
            next: (data: MessagesModel) => {
                if (data['mensajes'].length > 0) {
                    onlyMessages = JSON.parse(data['mensajes'][0]['jsonMensajes']);
                } else {
                    onlyMessages = [];
                }
            },
            error: (error) => { console.error('Error al consultar los mensajes con el servicio => ', error); },
            complete: () => {
                this.messagesArray = onlyMessages;
                setTimeout(() => {
                    const objDiv = document.querySelector('#principal-container > #container-msg-list');
                    objDiv.scrollTop = (objDiv.scrollHeight);
                }, 100);
            }
        });
    }

    // Método para insertar un nuevo chat en la base de datos
    // Si el no existe, quiere decir que es un chat nuevo y que se agregará como registro nuevo en la tabla de chats
    postMessagesChat(messagesArray) {
        const messages = JSON.stringify(messagesArray);
        this.messagesService.postMessagesChat(this.ticketID, messages).subscribe({
            next: (response) => { },
            error: (error) => { console.error('Error al insertar un nuevo registro en la base de datos => ', error); },
            complete: () => {
                this.emitNotification(messagesArray);
            }
        });

    }

    // Método para actualizar el chat que ya se encuentra en la tabla
    // Si el chat existe, entonces solamente se actualizarán los mensajes con el nuevo arreglo de mensajes
    putMessagesChat(messagesArray) {
        // console.info('Actualizando mensajes del chat existente');
        this.messagesService.putMessagesChat(this.ticketID, JSON.stringify(messagesArray)).subscribe({
            next: (response) => { },
            error: (error) => { console.error('Error al actualizar los datos del chat => ', error); },
            complete: () => { this.emitNotification(messagesArray); }
        });
    }

    // Metodo para emitir una notificacion de un nuevo mensaje
    private emitNotification(messagesArray) {
        this.socket.emit('notificacion', {
            typenotification: 'newmessage',
            message: messagesArray[messagesArray.length - 1].message,
            sender: this.employeeInfo['idEmpleado'],
            reciever: this.receiver,
            photo: this.profile['profileImage'],
            ticketid: this.ticketID,
            ticketstatus: this.ticketStatus
        });
    }

    // Método para subir la imagen al servidor backend
    saveImage(file: File, ticketID: number) {
        let nameImage = `P${this.profile['profileid']}-D${Date.now()}`;
        this.messagesService.uploadImage(nameImage, file, ticketID).subscribe({
            next: (data) => { },
            error: (error) => { console.error('Error Data Imagen Class => ', error); },
            complete: () => {
                this.imageurl = `chat${ticketID}/${nameImage}.${file.type.split('/')[1]}`;
                if (this.ticketID > 0) {
                    this.SendMessage();
                }
            }
        });
    }

    // Método para llenar un arreglo de imágenes que son las que se mostrarán en el modal
    getOnlyImages() {
        const imagesArray: Object[] = [];
        let indice = 0;
        for (let i = 0; i < this.messagesArray.length; i++) {
            if (this.messagesArray[i].image.trim() !== '') {
                imagesArray.push({ index: indice, image: this.messagesArray[i].image });
                indice += 1;
            }
        }
        return imagesArray;
    }

}

