import { Component, OnInit, ViewChild } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator, MatPaginatorIntl } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { EmployeesPermission } from 'src/app/@core/Models/Employees/EmployeesPermissions@Model';
import { EmployeesService } from 'src/app/@core/Services/Employess/employees.service';
import { UserTypesService } from 'src/app/@core/Services/UserTypes/user-types.service';
import { UsuarioService } from 'src/app/@core/Services/usuario.service';
import { ManageProfilesComponent } from '../manage-profiles/manage-profiles.component';

import Swal from 'sweetalert2';
import { PermissionsModalComponent } from 'src/app/shared/permissions/permissions-modal.component';

@Component({
    selector: 'app-ajustes-perfiles',
    templateUrl: 'ajustes-perfiles.component.html',
    styleUrls: ['ajustes-perfiles.component.scss']
})

export class AjustesPerfilesComponent implements OnInit {

    dataSource: MatTableDataSource<EmployeesPermission>;
    displayedColumns: string[]; // = ['numEmpleado', 'vchNombre', 'vchSede', 'vchTipoUsuario', 'sesionActiva'];
    @ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChild(MatSort) sort: MatSort;

    formGroup: FormGroup;

    arrayUserDataSource = [];

    userTypes = [];

    loaderActivated: boolean = true;

    constructor(
        private pagin: MatPaginatorIntl,
        private formBuilder: FormBuilder,
        private employeesService: EmployeesService,
        private dialog: MatDialog,
        private userTypesService: UserTypesService,
        private usuarioService: UsuarioService
    ) {
        this.renameTableInfo();
        this.getuserTypes();
        this.startFormBuild();
        this.userTypeField.setValue(1);
        this.fillDisplayColumns();
        this.getEmployeesByUserTypeID();
        this.dataSource = new MatTableDataSource();
    }

    private fillDisplayColumns() {
        switch (this.userTypeField.value) {
            case 1:
                this.displayedColumns = ['numEmpleado', 'vchNombre', 'vchSede', 'vchTipoUsuario', 'cuentaActiva', 'ajustesUsuario', 'ajustesMenus'];
                break;
            case 2:
                this.displayedColumns = ['numEmpleado', 'vchNombre', 'vchSede', 'vchTipoUsuario', 'cuentaActiva', 'ajustesUsuario', 'ajustesMenus'];
                break;
            case 3:
                this.displayedColumns = ['numEmpleado', 'vchNombre', 'vchSede', 'vchTipoUsuario', 'areaAtencion', 'sesionActiva', 'cuentaActiva', 'ajustesUsuario', 'ajustesMenus'];
                break;
        }
    }

    private renameTableInfo() {
        this.pagin.itemsPerPageLabel = 'Registros por página';
        this.pagin.firstPageLabel = 'Primera página';
        this.pagin.lastPageLabel = 'Última página';
        this.pagin.nextPageLabel = 'Página siguiente';
        this.pagin.previousPageLabel = 'Página anterior';
    }

    ngOnInit() { }

    applyFilter(evt) {
        const searchValue = (evt.target as HTMLInputElement).value;
        this.dataSource.filter = searchValue.trim().toLocaleLowerCase();
        if (this.dataSource.paginator) {
            this.dataSource.paginator.firstPage();
        }
    }

    // Obtener los empleados de acuerdo al tipo de usuario seleccionado
    getEmployeesByUserTypeID() {
        let employeesServiceResponse;
        this.employeesService.getEmployeesByUserTypeID(this.userTypeField.value).subscribe({
            next: (response) => {
                employeesServiceResponse = Object.values(response['employeesByUserType'][0]);
            },
            error: (error) => {
                console.error('Error al consultar los empleados por tipo de usuario => ', error);
            },
            complete: () => {
                this.loaderActivated = false;
                this.arrayUserDataSource = employeesServiceResponse;
                this.dataSource = new MatTableDataSource(employeesServiceResponse);
                this.dataSource.sort = this.sort;
                this.dataSource.paginator = this.paginator;
            }
        });
    }

    changeUserType() {
        this.loaderActivated = true;
        this.fillDisplayColumns();
        this.getEmployeesByUserTypeID();
    }

    openManageProfileModal(empleadoID: number, tipoUsuarioID: number, areaAtencionID: number) {
        this.dialog.open(ManageProfilesComponent, {
            data: {
                empleadoID,
                tipoUsuarioID,
                areaAtencionID
            },
            panelClass: 'editarTicketModal',
            width: '500px',
            height: '350px', disableClose: true
        }).afterClosed().subscribe(response => {
            if (response.changes) {
                this.getEmployeesByUserTypeID();
            }
        });
    }

    permissionsSettings(employeeID: number) {
        this.dialog.open(PermissionsModalComponent, {
            data: {
                employeeID
            },
            panelClass: 'editarTicketModal',
            width: '500px',
            height: '550px',
            disableClose: true
        });
    }

    // Método para obtener los tipos de usuario
    private getuserTypes() {
        let userTypesResponse = [];
        this.userTypesService.getAllUserTypes().subscribe({
            next: (response) => {
                userTypesResponse = response['tipoUsuarios'];
            },
            error: (error) => {
                console.error('Error al consultar los tipos de usuarios');
            },
            complete: () => {
                this.userTypes = userTypesResponse;
            }
        })
    }

    // Método para actualizar el estado de la sesion
    changeStatusSession(evt, email, method: boolean) {
        this.usuarioService.updateSessionActive(email, evt.checked).subscribe({
            next: (response) => { },
            error: (error) => {
                console.error('Error al actualizar el estado de la sesion de este usuario');
            },
            complete: () => {
                this.arrayUserDataSource.find(e => e.email === email).blnSesionActiva = evt.checked;
                if (method === false) {
                    this.dataSource = new MatTableDataSource(this.arrayUserDataSource);
                    this.dataSource.sort = this.sort;
                    this.dataSource.paginator = this.paginator;
                    Swal.fire({
                        title: '¡Actualización exitosa!',
                        text: evt.checked === false ? '¡Sesión desactivada!' : '¡Sesión activada exitosamente!',
                        icon: 'success'
                    });
                }

            }
        });
    }

    // Método para cambiar el estado de la cuenta
    changeStatusAccount(evt, empleadoID: number) {
        this.employeesService.putEmployeeStatausAccount(empleadoID, evt.checked).subscribe({
            next: (response) => { },
            error: (error) => {
                console.error('Error al actualizar el estado de la cuenta de este usuario');
            },
            complete: () => {
                this.arrayUserDataSource.find(e => e.idEmpleado === empleadoID).blnCuentaActiva = evt.checked;
                const email = this.arrayUserDataSource.find(e => e.idEmpleado === empleadoID).email;
                this.changeStatusSession(evt, email, true);
                this.dataSource = new MatTableDataSource(this.arrayUserDataSource);
                this.dataSource.sort = this.sort;
                this.dataSource.paginator = this.paginator;
                Swal.fire({
                    title: '¡Actualización exitosa!',
                    text: evt.checked === false ? '¡Cuenta desactivada!' : '¡Cuenta activada exitosamente!',
                    icon: 'success'
                });
            }
        })

    }

    // Método para cargar el formBuilder
    startFormBuild() {
        this.formGroup = this.formBuilder.group({
            userType: [null, [Validators.required]]
        });
    }

    // Propiedades get para acceder a las propiedades del formGroup
    get userTypeField(): AbstractControl {
        return this.formGroup.get('userType');
    }

}