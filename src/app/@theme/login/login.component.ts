import { Component, NgZone, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { EmployeesService } from 'src/app/@core/Services/Employess/employees.service';
import { UsuarioService } from '../../@core/Services/usuario.service';

import Swal from 'sweetalert2';

declare const gapi: any;

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  usersFormGroup: FormGroup;
  generalData: any;
  public auth2: any;

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private usuarioService: UsuarioService,
    private ngZone: NgZone,
    private employeesService: EmployeesService
  ) {
  }

  ngOnInit(): void {
    this.usersFormGroup = this.formBuilder.group({
      nombre: [null, [Validators.required]],
      password: [null, [Validators.required]],
    });
    this.renderButton();
  }

  renderButton() {
    gapi.signin2.render('my-signin2', {
      scope: 'profile email',
      width: 'auto',
      height: 50,
      longtitle: false,
      theme: 'dark',
    });

    this.startApp();
  }

  async startApp() {
    await this.usuarioService.googleInit();
    this.auth2 = this.usuarioService.auth2;

    this.attachSignin(document.getElementById('my-signin2'));
  }

  attachSignin(element) {
    this.auth2.attachClickHandler(element, {},
      (googleUser) => {
        const id_token = googleUser.getAuthResponse().id_token;
        const profile = googleUser.getBasicProfile();
        const arrayProfile = { profileid: profile.getId(), profilename: profile.getName(), profileImage: profile.getImageUrl(), profileemail: profile.getEmail() };

        this.usuarioService.loginGoogle(id_token)
          .subscribe(resp => {
            localStorage.setItem('profile', JSON.stringify(arrayProfile));
            if (resp.ok === true) {
              // Navegar al Dashboard
              this.getEmployeeLoginInfo(profile.getEmail());
            } else if (resp.ok === false) {
              localStorage.removeItem('token');
              localStorage.removeItem('profile');
              Swal.fire({
                title: '<strong>Cuenta Inactiva</strong>',
                html: 'Su cuenta se encuentra desactivada.<br>Cumuníquese con el administrador para reactivarla.',
                icon: 'error'
              });
            } else if (resp.exists === false) {
              this.showModalToIDEmployeeEnter(profile.getEmail());
            }
          });

      }, (error) => {
        alert(JSON.stringify(error, undefined, 2));
      });
  }

  loginFunction(): any {
    this.router.navigate(['/modulos/dashboard']);
  }

  private getEmployeeLoginInfo(email: string) {
    let dataResponse;
    let tipoUsuario;
    this.employeesService.getEmployeeByEmail().subscribe({
      next: (data) => {
        dataResponse = data;
      },
      error: (error) => { console.error('Error al consultaro los datos => ', error); },
      complete: () => {
        const values = Object.values(dataResponse['empleado'][0]);
        if (values.length > 0) {
          tipoUsuario = dataResponse['empleado'][0][0].fkIdTipoUsuario;
          localStorage.setItem('employeeInfo', JSON.stringify(dataResponse['empleado'][0][0]));
          this.updateStatusSession(email, tipoUsuario);
          // this.router.navigateByUrl('/modulos/dashboard');
          this.ngZone.run(() => {
            this.router.navigate(['/modulos/dashboard']);
          });
        } else {
          this.showModalToIDEmployeeEnter(email);
        }
      }
    });
  }

  showModalToIDEmployeeEnter(email: string) {
    Swal.fire({
      title: 'Ingrese su ID de Empleado',
      html: 'Es la primera vez que está iniciando sesión, ingrese su <br><strong>ID de Empleado</strong> para continuar y configurar su perfil.',
      input: 'text',
      inputAttributes: {
        autocapitalize: 'off'
      },
      showCancelButton: true,
      cancelButtonText: 'Cancelar',
      confirmButtonText: 'Aceptar',
      showLoaderOnConfirm: true,
      preConfirm: (result) => {
        let dataRes;
        return this.employeesService.postNewEmployee(email, result).subscribe({
          next: (data) => {
            dataRes = data;
          },
          error: (error) => {
            console.error('error al insertar');
          },
          complete: () => {
            return dataRes;
          }
        });
      },
      allowOutsideClick: () => !Swal.isLoading()
    }).then((result) => {
      if (result.isConfirmed) {
        Swal.fire({
          title: 'Registro Exitoso',
          text: '¡Su usuario se registró exitosamente!',
          icon: 'success'
        });

        this.getEmployeeLoginInfo(email);
      };
    });
  }

  updateStatusSession(email: string, tipoUsuario: number) {
    if (tipoUsuario === 3) {
      this.usuarioService.updateSessionActive(email, true).subscribe({
        next: (data) => { },
        error: (error) => { console.error('Error al update status => ', error); },
        complete: () => { }
      });
    }
  }

}
