import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {LevantarTicketComponent} from './levantar-ticket/levantar-ticket.component';

const routes: Routes = [
  {path: '', component: LevantarTicketComponent},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LevantarTicketRoutingModule { }
