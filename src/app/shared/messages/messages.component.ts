import { Component, ElementRef, HostListener, Inject, OnInit, ViewChild } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ChatClass } from 'src/app/@core/Classes/Chat/Chat@Class';
import { ViewImagesClass } from 'src/app/@core/Classes/Chat/ViewImages@Class';
import { MessagesService } from 'src/app/@core/Services/Messages/messages.service';
import { environment } from 'src/environments/environment';
import { ModalComponent } from '../../modulos/my-tickets/modal-component/modal/modal.component';

@Component({
    selector: 'app-messages-modal',
    templateUrl: 'messages.component.html',
    styleUrls: ['messages.component.scss']
})

export class MessagesComponent implements OnInit {

    // Instancia de la clase de chat donde se desarrolla toda la lógica
    chatClass: ChatClass;

    // Instancia de la clase para las imagenes en el carrusel
    viewImgClass: ViewImagesClass;

    // Atributo para validar el estado del Estado del Ticket
    ticketStatus: number;

    // Atributo para ocultar o desocultar el div principal del chat
    hiddenChat: boolean = false;

    // Atributo para almacenar la imagen y previsualizarla
    blobImage: any;

    // Atriburo para acceder a la base url del environment
    baseURL = environment.baseURL;

    // Para acceder al input file y seleccionar la imagen
    @ViewChild('file') file: ElementRef;

    // Para acceder al img
    @ViewChild('imagePreview') preview: ElementRef;

    // Atributo para almacenar la imagen
    fileImage: File;

    constructor(
        private messagesService: MessagesService,
        private dialog: MatDialog,
        public dialogRef: MatDialogRef<MessagesComponent>,
        @Inject(MAT_DIALOG_DATA) public dataExternalDialog: any
    ) {
        this.ticketStatus = this.dataExternalDialog.ticketStatus;
        this.chatClass = new ChatClass(this.dataExternalDialog.ticketId, this.messagesService, this.dataExternalDialog.receiver, this.ticketStatus, false);
        this.viewImgClass = new ViewImagesClass(this.dialog);
        this.chatClass.setupSocketConnection();
    }

    ngOnInit() { }

    closeMessageDialog() {
        localStorage.setItem('chatOpen', 'false');
        this.dialogRef.close();
        this.chatClass.closeSocketConnection();
    }

    searchImage() {
        const fielSearch = this.file.nativeElement;
        fielSearch.click();
    }

    onFileChange(event: any) {
        if (event.target.files && event.target.files.length) {
            const file = event.target.files[0];
            if (file.type.includes('image')) {
                const imgPrev = this.preview.nativeElement;
                const url = URL.createObjectURL(file);
                imgPrev.src = url;
                this.hiddenChat = true;
                this.fileImage = file;
            }
        }
    }

    saveImage() {
        this.chatClass.saveImage(this.fileImage, this.dataExternalDialog.ticketId);
        this.file.nativeElement.value = '';
        this.hiddenChat = false;
    }

    goBack() {
        this.hiddenChat = false;
        this.file.nativeElement.value = '';
    }

    /* Método para captura el evento de la tecla que se está presionando dentro del documento;
     solo aplicará cuando la tecla que se presione sea el esc que es para cerrar el modal del chat*/
    @HostListener('document:keydown', ['$event'])
    keyPressGlobal(event: KeyboardEvent): void {
        if (event.keyCode === 27) {
            this.closeMessageDialog();
        }
    }

    viewImages(selectedImage: string) {
        const imagesArray = this.chatClass.getOnlyImages();
        this.viewImgClass.viewImages(imagesArray, selectedImage);
    }

}
