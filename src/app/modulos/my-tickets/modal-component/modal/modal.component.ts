import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.scss']
})
export class ModalComponent implements OnInit {

  dataTicket: any;

  constructor(
    public matDialogRef: MatDialogRef<ModalComponent>,
    @Inject(MAT_DIALOG_DATA) public dataExternalDialog: any
  ) {
    this.dataTicket = this.dataExternalDialog.itemTicket;
  }

  ngOnInit(): void { }

  closeModal(): any {
    this.matDialogRef.close();
  }

}
