import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {AtencionTicketsComponent} from './atencion-tickets/atencion-tickets.component';

const routes: Routes = [
  {path: '', component: AtencionTicketsComponent},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AtencionTicketsRoutingModule { }
