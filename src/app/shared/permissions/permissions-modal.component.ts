import { Component, ElementRef, Inject, OnInit, ViewChild } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MenusAddedModel } from 'src/app/@core/Models/Menus/MenusAdded@Model';
import { MenusNotAddedModel } from 'src/app/@core/Models/Menus/MenusNotAdded@Model';
import { MenusService } from 'src/app/@core/Services/Menus/menus.service';

import Swal from 'sweetalert2';

@Component({
    selector: 'app-permissions-modal',
    templateUrl: 'permissions-modal.component.html',
    styleUrls: ['permissions-modal.component.scss']
})

export class PermissionsModalComponent implements OnInit {

    employeeID: number;
    menusAdded: MenusAddedModel[];
    menusNotAdded: MenusNotAddedModel[];

    valuesToUpdate = [];
    valuesNewMenusToAdd = [];

    @ViewChild('tabGroup') tabGroup;

    constructor(
        private matDialogRef: MatDialogRef<PermissionsModalComponent>,
        @Inject(MAT_DIALOG_DATA) public dataExternalDialog: any,
        private menusService: MenusService
    ) {
        this.employeeID = parseInt(this.dataExternalDialog.employeeID);
        this.getMenusAdded();
        this.getMenusNotAdded();
    }

    ngOnInit() { }

    closeDialog() {
        this.matDialogRef.close();
    }

    getMenusAdded() {
        let dataResponse: any;
        this.menusService.getmenusAdded(this.employeeID).subscribe({
            next: (data) => { dataResponse = data; },
            error: (error) => { console.error('Error al consultar los menus agregados => ', error); },
            complete: () => { this.menusAdded = Object.values(dataResponse['menusAdded'][0]); }
        });
    }

    getMenusNotAdded() {
        let dataResponse: any;
        this.menusService.getMenusNotAdded(this.employeeID).subscribe({
            next: (data) => { dataResponse = data; },
            error: (error) => { console.error('Error al consultar los menus no agregados => ', error); },
            complete: () => { this.menusNotAdded = Object.values(dataResponse['menusNotAdded'][0]); }
        });
    }

    changeMenusAdded(value: any, selected: boolean) {
        this.menusService.putMenusStatusActive({ idEmpleado: this.employeeID, idMenu: value.idMenu, status: selected }).subscribe({
            next: (response) => { },
            error: (error) => { console.error('Ocurrió un error al actualizar el permiso del menu', error); },
            complete: () => {
                this.getMenusAdded();
            }
        });
    }

    changeMenusNotAdded(value: any, selected: boolean) {
        this.menusService.postMenusToEmployee({ idEmpleado: this.employeeID, idMenu: value.idMenu, status: selected }).subscribe({
            next: (response) => { },
            error: (error) => { console.error('Ocurrió un error al insertar el menu', error); },
            complete: () => {
                this.getMenusNotAdded();
            }
        });

    }

    changeTab(evt) {
        if (evt.index === 0) {
            this.getMenusAdded();
        } else {
            this.getMenusNotAdded();
        }
    }

}