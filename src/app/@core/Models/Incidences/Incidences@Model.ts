export interface IncidencesModel {
    idIncidencia: number;
    vchDescripcion: string;
    blnActivo: boolean;
}