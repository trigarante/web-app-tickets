import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator, MatPaginatorIntl } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { TicketsService } from 'src/app/@core/Services/Tikcets/tickets.service';
import { AjustesTicketsModalComponent } from '../ajustes-tickets-modal/ajustes-tickets-modal.component';
import { AssignTicketsModalComponent } from '../assign-tickets-modal/assign-tickets-modal.component';
import { ModalDetailsComponent } from '../modal-details/modal-details.component';

@Component({
  selector: 'app-ajustes-tickets',
  templateUrl: './ajustes-tickets.component.html',
  styleUrls: ['./ajustes-tickets.component.scss']
})
export class AjustesTicketsComponent implements OnInit {

  sede: string;
  tabIndex: number = 0;

  loaderActivated: boolean = true;

  // Para los tickets con estados
  dataSource: MatTableDataSource<any>;
  displayedColumns = []; // ['ticketID', 'vchEmpleadoSolicita', 'vchIncidencia', 'vchEstadoTicket', 'vchAreaAtencion', 'vchSede', 'detalles', 'ajustes'];
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;



  /* @ViewChild(MatPaginator) paginatorNotAssigned: MatPaginator;
  @ViewChild(MatSort) sortNotAssigned: MatSort; */

  constructor(
    private pagin: MatPaginatorIntl,
    private ticketsService: TicketsService,
    private dialog: MatDialog
  ) {
    this.sede = JSON.parse(localStorage.getItem('employeeInfo')).vchSede;
    this.pagin.itemsPerPageLabel = 'Registros por página';
    this.pagin.firstPageLabel = 'Primera página';
    this.pagin.lastPageLabel = 'Última página';
    this.pagin.nextPageLabel = 'Página siguiente';
    this.pagin.previousPageLabel = 'Página anterior';
    this.changeTab({ index: 0 });
    this.getTicketsOnPending(1);
  }

  ngOnInit(): void { }

  changeTab(evt) {
    this.loaderActivated = true;
    switch (evt.index) {
      case 0:
        this.displayedColumns = ['ticketID', 'vchEmpleadoSolicita', 'vchIncidencia', 'vchEstadoTicket', 'vchAreaAtencion', 'vchSede', 'detalles', 'ajustes'];
        this.tabIndex = 0;
        this.getTicketsOnPending(1);
        break;
      case 1:
        this.displayedColumns = ['ticketID', 'vchEmpleadoSolicita', 'vchIncidencia', 'vchEstadoTicket', 'vchAreaAtencion', 'vchSede', 'detalles', 'ajustes'];
        this.tabIndex = 1;
        this.getTicketsOnPending(2);
        break;
      case 2:
        this.displayedColumns = ['ticketID', 'vchEmpleadoSolicita', 'vchIncidencia', 'vchEstadoTicket', 'vchAreaAtencion', 'vchSede', 'detalles', 'ajustes', 'reabrir'];
        this.tabIndex = 2;
        this.getTicketsOnPending(3);
        break;
    }
  }

  getTicketsOnPending(status) {
    // const status = 1;
    let ticketsResponse;
    this.ticketsService.getTicketsByStatus(this.sede, status).subscribe({
      next: (data) => {
        ticketsResponse = Object.values(data['ticketsByStatus'][0]);
      },
      error: (error) => {
        console.error('Error al consultar los tickets por estados => ', error);
      },
      complete: () => {
        this.loaderActivated = false;
        this.dataSource = new MatTableDataSource(ticketsResponse);
        this.dataSource.sort = this.sort;
        this.dataSource.paginator = this.paginator;
      }
    });
  }

  applyFilter(evt: Event) {
    const searchValue = (evt.target as HTMLInputElement).value;
    this.dataSource.filter = searchValue.trim().toLocaleLowerCase();
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  openDetailModal(item: any, exists: boolean) {
    this.dialog.open(ModalDetailsComponent, {
      data: {
        itemTicket: item,
        ticketExistente: exists
      },
      disableClose: true,
      panelClass: 'customModalAtencionTickets',
    });
  }

  openDialogSettingTickets(ticketID: number, idAreaAtencion: number) {
    this.dialog.open(AjustesTicketsModalComponent, {
      data: { ticketID, idAreaAtencion, tabIndex: this.tabIndex },
      panelClass: 'editarTicketModal',
      width: '500px',
      height: '350px',
      disableClose: true
    }).afterClosed().subscribe(result => {
      if (result.success) {
        const status = (this.tabIndex + 1);
        this.getTicketsOnPending(status);
      }
    });
  }

  reopen(ticketID: number) {
    const status = 2;
    this.ticketsService.putStateTicket(ticketID, status).subscribe({
      next: (data) => {
        console.log('Ticket reabierto => ', data);
      },
      error: (error) => {
        console.error('Error al reabrir el ticket => ', error);
      },
      complete: () => {
        this.getTicketsOnPending(3);
      }
    });
    console.log('reabrir ticket', ticketID);
  }

}
