import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { AtenderTickets } from '../../Models/Atender-Tickets/atender-tickets';
import { environment } from '../../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AtenderTicketsService {

  constructor(private http: HttpClient) { }
  getTicketsAtencionById(): Observable<AtenderTickets[]> {
    const employeeInfo = JSON.parse(localStorage.getItem('employeeInfo'));
    const sede = employeeInfo['vchSede'];
    const idEmpleado = employeeInfo['idEmpleado'];
    return this.http.get<AtenderTickets[]>(`${environment.baseURL}/atender-ticket/${idEmpleado}/${sede}`);
  }

  putEstadoTicket(noTicket: number, estado: number): Observable<AtenderTickets[]> {
    const arrayDataChat = { noTicket, estado };
    return this.http.put<AtenderTickets[]>(`${environment.baseURL}/atender-ticket`, arrayDataChat);
  }

  getAttendingEmployee(ticketID: number) {
    return this.http.get(`${environment.baseURL}/atender-ticket/get-employee/${ticketID}`);
  }

  postEmployeeTicket(info: any) {
    return this.http.post(`${environment.baseURL}/atender-ticket/set-employee-ticket`, info);
  }

}
