import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatDialog } from '@angular/material/dialog';
import { DetallesTicketAtencionModalComponent } from '../modals/detalles-ticket-atencion-modal/detalles-ticket-atencion-modal.component';
import { AtenderTicketModalComponent } from '../modals/atender-ticket-modal/atender-ticket-modal.component';
import { AtenderTicketsService } from '../../../@core/Services/Atender-Tickets/atender-tickets.service';
import { AtenderTickets } from '../../../@core/Models/Atender-Tickets/atender-tickets';
import { MessagesComponent } from 'src/app/shared/messages/messages.component';
import { io } from 'socket.io-client';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-atencion-tickets',
  templateUrl: './atencion-tickets.component.html',
  styleUrls: ['./atencion-tickets.component.scss']
})
export class AtencionTicketsComponent implements OnInit, AfterViewInit {

  displayedColumns: string[] = ['noTicket', 'incidencia', 'solicitante', 'fechaSolicitud', 'sede', 'estadoTicket', 'detalles', 'mensajes'];
  dataSource: MatTableDataSource<AtenderTickets>;
  ticketsResponse: any;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  ticketsDataSlice = this.getTicketsAtencionById();

  // Atributo donde se almacenará el socket
  socket: any;
  // Almacena los datos del empleado
  employeeInfo: any;
  // Almacena los datos del perfil de google de la perona que inició sesión
  profile: any;

  constructor(
    private dialog: MatDialog,
    private atencionTicketsServices: AtenderTicketsService,
  ) {
    this.profile = JSON.parse(localStorage.getItem('profile'));
    this.employeeInfo = JSON.parse(localStorage.getItem('employeeInfo'));
    this.setupSocketConnection();
    this.getTicketsAtencionById();
    this.getTicketsAtencionById();
    this.dataSource = new MatTableDataSource();
  }

  private setupSocketConnection() {
    this.socket = io(environment.SOCKET_ENDPOINT, {
      query: {
        token: ''
      }
    }), {
      // Se implementaron los permisos para cors
      withCredentials: true,
      extraHeaders: {
        "my-custom-header": "abcd"
      }
    };
  }

  ngAfterViewInit(): any {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  ngOnInit(): void {
    this.getTicketsAtencionById();
  }

  openDetailsTicketsAssitence(item: any): any {
    this.dialog.open(DetallesTicketAtencionModalComponent, {
      data: {
        itemTicket: item
      },
      disableClose: true,
      panelClass: 'customModalAtencionTickets',
    });
  }

  getTicketsAtencionById(): any {
    this.atencionTicketsServices.getTicketsAtencionById().subscribe({
      next: (resp) => {
        this.ticketsResponse = Object.values(resp['ticketAtendidos'][0]);   
      }, error: (err) => {
        console.error('Error al Obtener los registros: ', err);
      }, complete: () => {
        this.dataSource = new MatTableDataSource(this.ticketsResponse);
        this.dataSource.sort = this.sort;
        this.dataSource.paginator = this.paginator;
      }
    });
  }

  openAtentionModal(item: any): any {
    this.dialog.open(AtenderTicketModalComponent, {
      data: {
        itemTicket: item
      },
      disableClose: true,
      panelClass: 'customModalAtencionTickets',
    }
    ).afterClosed().subscribe(data => {
      if (data.estadoTicket !== undefined) {
        this.ticketsResponse.find(e => e.fkIdCreacionTicket === data.idTicket).idEstadoTicket = data.estadoTicket;
        this.ticketsResponse.find(e => e.fkIdCreacionTicket === data.idTicket).vchDescripcionEstadoTickets = data.descripcionEstadoTicket;
        const idEmpleadoSolicita = this.ticketsResponse.find(e => e.fkIdCreacionTicket === data.idTicket).idEmpleado;
        this.dataSource = new MatTableDataSource(this.ticketsResponse);
        this.dataSource.sort = this.sort;
        this.dataSource.paginator = this.paginator;
        this.emitStatusChange(data.estadoTicket, data.idTicket, data.descripcionEstadoTicket);
        if (data.changedStatus) {
          this.emitNotification(data.estadoTicket, data.idTicket, idEmpleadoSolicita);
        }
      }
    });
  }

  private emitNotification(estadoTicket: number, idticket: number, idEpleadoSolicita: number) {
    let descriptionMessage = estadoTicket === 2 ? `Tu ticket #${idticket} está En Proceso` : `Tu ticket #${idticket} ha sido Cerrado`;
    this.socket.emit('notificacion', {
      typenotification: 'changestatus',
      message: descriptionMessage,
      sender: '',
      reciever: idEpleadoSolicita,
      photo: this.profile['profileImage'],
      ticketid: idticket,
      ticketstatus: estadoTicket
    });
  }

  private emitStatusChange(newStatus: number, ticketID: number, descripcionEstadoTicket: string) {

    const receiver = this.ticketsResponse.find(e => e.fkIdCreacionTicket === ticketID).idEmpleado;

    this.socket.emit('statusChange', {
      newstatus: newStatus,
      ticketid: ticketID,
      sender: this.employeeInfo['idEmpleado'],
      receiver: receiver,
      statusticketdesc: descripcionEstadoTicket
    });
  }

  applyFilter(event: Event): any {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  openDialogMessages(ticketID: number, ticketStatus: number, empleadoSolicita: number) {
    const chatOpen = localStorage.getItem('chatOpen');
    if (chatOpen === 'false') {
      localStorage.setItem('chatOpen', 'true');
      this.dialog.open(MessagesComponent, {
        panelClass: 'messengerStylesModal',
        data: {
          ticketId: ticketID,
          ticketStatus: ticketStatus,
          receiver: empleadoSolicita
        },
        disableClose: true
      });
    }
  }

}
