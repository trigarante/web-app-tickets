export interface TicketsViewModel {
    idCreacionTicket: number;
    fechaSolicitud: string;
    idEstadoTicket: number;
    statusDescription: string;
    incidencesDescription: string;
    vchNombre: string;
    vchApellidoPaterno: string;
    vchApellidoMaterno: string;
    incidencesDetails: string;
    txtDescripcion: string;
    fkIdEmpleadoSolicita: number;
}
