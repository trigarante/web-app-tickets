export interface EmployeesPermission {
    idEmpleado: number;
    vchNombre: string;
    vchApellidoPaterno: string;
    vchApellidoMaterno: string;
    fkIdEmpleado: number;
    vchSede: string;
    vchTipoUsuario: string;
}