import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatCarouselComponent } from '@ngbmodule/material-carousel';
import { environment } from 'src/environments/environment';
@Component({
    selector: 'app-images-carrousel-chat',
    templateUrl: 'images-carrousel-chat.component.html',
    styleUrls: ['images-carrousel-chat.component.scss']
})

export class ImagesCarrouselChatComponent implements OnInit {

    imagesArray: any[];
    selectedImage: number;
    imageDefault: string;

    urlDownloadImage: string = `${environment.baseURL}/images/download/`;

    constructor(
        public dialogRef: MatDialogRef<ImagesCarrouselChatComponent>,
        @Inject(MAT_DIALOG_DATA) public dataExternalDialog: any
    ) {
        this.imagesArray = this.dataExternalDialog.imagesArray;
        this.imageDefault = this.dataExternalDialog.selectedImage;
        this.selectedImage = this.selectedImagenDefault();
    }

    ngOnInit() { }

    closeDialog() {
        this.dialogRef.close();
    }

    slideConfig = { "slidesToShow": 10, "slidesToScroll": 10 };

    onImageClick(i) {
        this.selectedImage = i;
    }

    selectedImagenDefault() {
        const indexDefault = this.imagesArray.find(item => item.image === this.imageDefault).index;
        return indexDefault;
    }

}
