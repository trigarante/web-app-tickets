import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LevantarTicketComponent } from './levantar-ticket.component';

describe('LevantarTicketComponent', () => {
  let component: LevantarTicketComponent;
  let fixture: ComponentFixture<LevantarTicketComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LevantarTicketComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LevantarTicketComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
