import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { TicketsViewModel } from '../../Models/tickets/tickets@Model';
import { Observable } from 'rxjs';

@Injectable({ providedIn: 'root' })
export class TicketsService {

    constructor(
        private http: HttpClient
    ) { }

    getTickets() {

    }

    postTickets(ticketArray: any[]) {
        return this.http.post(`${environment.baseURL}/tickets`, ticketArray);
    }

    getViewTickets(): Observable<TicketsViewModel[]> {
        const employeeInfo = JSON.parse(localStorage.getItem('employeeInfo'));
        const idEmpleado = employeeInfo['idEmpleado'];
        return this.http.get<TicketsViewModel[]>(`${environment.baseURL}/tickets/${idEmpleado}`)
    }

    getTicketsByStatus(sede: string, status: number) {
        return this.http.get(`${environment.baseURL}/tickets/by-status/${sede}/${status}`);
    }

    getTicketsNotAttending(sede: string) {
        return this.http.get(`${environment.baseURL}/tickets/not-assigned/${sede}`);
    }

    putAtentionInfoByTicketID(ticketID: number, dataToUpdate: any) {
        return this.http.put(`${environment.baseURL}/tickets/update-atention-info/${ticketID}`, dataToUpdate);
    }

    putStateTicket(ticketID: number, status: number) {

        return this.http.put(`${environment.baseURL}/tickets/update-state-ticket/${ticketID}`, { status: status });
    }

}

