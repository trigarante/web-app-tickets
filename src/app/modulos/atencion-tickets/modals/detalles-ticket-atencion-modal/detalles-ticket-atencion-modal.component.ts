import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';

@Component({
  selector: 'app-detalles-ticket-atencion-modal',
  templateUrl: './detalles-ticket-atencion-modal.component.html',
  styleUrls: ['./detalles-ticket-atencion-modal.component.scss']
})
export class DetallesTicketAtencionModalComponent implements OnInit {
  dataTicket: any;
  constructor(
    private matDialogRef: MatDialogRef<DetallesTicketAtencionModalComponent>,
    @Inject(MAT_DIALOG_DATA) public dataExternalDialog: any
    ) {
    this.dataTicket = this.dataExternalDialog.itemTicket;
  }

  ngOnInit(): void {
  }
  closeModal(): any {
    this.matDialogRef.close();
  }
}
