import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AjustesPerfilesComponent } from './components/ajustes-perfiles/components/ajustes-perfiles/ajustes-perfiles.component';
import { AjustesTicketsComponent } from './components/ajustes-tickets/components/ajustes-tickets/ajustes-tickets.component';
import { PermisosComponent } from './components/permisos/permisos.component';


const routes: Routes = [
  { path: 'permisos', component: PermisosComponent },
  { path: 'ajustes-tickets', component: AjustesTicketsComponent },
  { path: 'ajustes-perfiles', component: AjustesPerfilesComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }