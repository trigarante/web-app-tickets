import { AfterViewInit, Component, ViewChild, OnInit } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { MatDialog } from '@angular/material/dialog';
import { ModalComponent } from '../../modal-component/modal/modal.component';
import { TicketsService } from '../../../../@core/Services/Tikcets/tickets.service';
import { TicketsViewModel } from '../../../../@core/Models/tickets/tickets@Model';
import { MessagesComponent } from '../../../../shared/messages/messages.component';
import { io } from 'socket.io-client';
import { environment } from 'src/environments/environment';

import Swal from 'sweetalert2';

@Component({
  selector: 'app-my-tickets',
  templateUrl: './my-tickets.component.html',
  styleUrls: ['./my-tickets.component.scss']
})
export class MyTicketsComponent implements OnInit, AfterViewInit {
  displayedColumns: string[] = ['idTicket', 'incidence', 'date', 'state', 'details', 'messages'];
  dataSource: MatTableDataSource<TicketsViewModel>;
  ticketsResponse: any;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  // Atributo donde se almacenará el socket
  socket: any;

  constructor(
    public dialog: MatDialog,
    private ticketsService: TicketsService
  ) {
    this.connectionSocketInit();
    this.viewTickets();
    this.dataSource = new MatTableDataSource();
  }

  private connectionSocketInit() {
    this.socket = io(environment.SOCKET_ENDPOINT, {
      query: {
        token: ''
      }
    }), {
      // Se implementaron los permisos para cors
      withCredentials: true,
      extraHeaders: {
        "my-custom-header": "abcd"
      }
    };

    this.socket.on('statusChange', (data) => {
      if (data) {
        const newStatusticket = data.newstatusticket;
        const ticketId = data.ticketid;
        const receiver = parseInt(data.receiver);
        const statusticketdesc = data.statusticketdesc;
        const idEmpleado = parseInt(JSON.parse(localStorage.getItem('employeeInfo')).idEmpleado);
        if (receiver === idEmpleado) {
          this.ticketsResponse.find(e => e.idCreacionTicket === ticketId).idEstadoTicket = newStatusticket;
          this.ticketsResponse.find(e => e.idCreacionTicket === ticketId).statusDescription = statusticketdesc;
          this.dataSource = new MatTableDataSource(this.ticketsResponse);
          this.dataSource.paginator = this.paginator;
          this.dataSource.sort = this.sort;
        }
      }
    });

  }

  ngAfterViewInit() {
    setTimeout(() => {
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    }, 1000);
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
  ngOnInit(): void { }

  openDialog(item: any) {
    const dialogRef = this.dialog.open(ModalComponent, {
      panelClass: 'detailsModal',
      data: {
        itemTicket: item
      },
      disableClose: true
    });
  }

  viewTickets() {
    this.ticketsService.getViewTickets().subscribe({
      next: (response) => {
        this.ticketsResponse = response['viewTickets'];
      },
      error: (error) => { console.error('Error al consultar las vistas de tickets ', error) },
      complete: () => {
        this.dataSource = new MatTableDataSource(this.ticketsResponse);
        this.dataSource.sort = this.sort;
      }
    });
  }

  openDialogMessages(ticketID: number, ticketStatus: number, empleadoAtiende: number) {
    if (empleadoAtiende === null) {
      Swal.fire({
        title: 'Ticket no asignado',
        text: 'Comunícate con el Administrador para que te asigne un asesor y pueda atender tu ticket.',
        icon: 'error',
      });
    } else {
      const chatOpen = localStorage.getItem('chatOpen');
      if (chatOpen === 'false') {
        localStorage.setItem('chatOpen', 'true');
        this.dialog.open(MessagesComponent, {
          panelClass: 'messengerStylesModal',
          data: {
            ticketId: ticketID,
            ticketStatus: ticketStatus,
            receiver: empleadoAtiende
          },
          disableClose: true
        });
      }
    }
  }

}

