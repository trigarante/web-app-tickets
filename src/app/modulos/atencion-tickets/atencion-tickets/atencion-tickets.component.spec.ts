import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AtencionTicketsComponent } from './atencion-tickets.component';

describe('AtencionTicketsComponent', () => {
  let component: AtencionTicketsComponent;
  let fixture: ComponentFixture<AtencionTicketsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AtencionTicketsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AtencionTicketsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
