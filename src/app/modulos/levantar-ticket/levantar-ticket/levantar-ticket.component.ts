import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { IncidensesService } from 'src/app/@core/Services/Incidences/incidences.service';
import { IncidencesModel } from 'src/app/@core/Models/Incidences/Incidences@Model';
import { IncidencesDetailsModel } from 'src/app/@core/Models/Incidences/IncidencesDetails@Model';
import { EmployeesModel } from 'src/app/@core/Models/Employees/Employees@Model';
import { EmployeesService } from 'src/app/@core/Services/Employess/employees.service';
import { FormGroupTicket } from 'src/app/@core/Classes/FormGroups/Ticket/FormGroupTicket@Class';
import { TicketsService } from 'src/app/@core/Services/Tikcets/tickets.service';
import { AtentionAreasService } from 'src/app/@core/Services/AtentionAreas/atentionareas.service';
import { AtentionAreasModel } from 'src/app/@core/Models/AtentionAreas/AtentionAreas@Model';
import Swal from 'sweetalert2';
import { ChatClass } from 'src/app/@core/Classes/Chat/Chat@Class';
import { MessagesService } from 'src/app/@core/Services/Messages/messages.service';
import { AtenderTicketsService } from 'src/app/@core/Services/Atender-Tickets/atender-tickets.service';
import { MatDialog } from '@angular/material/dialog';
import { EditarTicketComponent } from 'src/app/shared/editar-ticket/editar-ticket.component';

interface Option {
  value: number;
  viewValue: string;
}

@Component({
  selector: 'app-levantar-ticket',
  templateUrl: './levantar-ticket.component.html',
  styleUrls: ['./levantar-ticket.component.scss']
})
export class LevantarTicketComponent implements OnInit {

  panelOpenState = false;
  incidencesArray: IncidencesModel[];
  incidencesDetailsArray: IncidencesDetailsModel[];
  employeesArray: EmployeesModel[];
  atentionAreasArray: AtentionAreasModel[];
  ticketsListFront = [];
  ticketsListBack = [];

  formGroupTicket: FormGroupTicket;

  // Booleanos para habilitar y deshabilitar los select
  enabledIncidenceDetails: boolean = true;
  enabledIncidence: boolean = true;
  enabledButtonAddTicket: boolean = false;
  enabledTextAreaDescription: boolean = true;
  enabledAtentionArea: boolean = true;
  enableOptionalMessage: boolean = true;
  enableOptionalImageButton: boolean = true;
  // Atributo para acceder al file
  @ViewChild('file') file: ElementRef;
  fileImage: File;
  arrayImages = [];
  // Instancia de la clase de Chat
  chatClass: ChatClass;
  //Arreglo de mensajes
  messagesArray = [];
  // Almacena los datos del perfil de google de la perona que inició sesión
  profile: any;
  // Información del empleado del localStorage
  sedeSolicita: any;

  idEmployeeSelected: number;

  constructor(
    private messagesService: MessagesService,
    private incidencesService: IncidensesService,
    private employeesService: EmployeesService,
    private ticketsService: TicketsService,
    private formBuilder: FormBuilder,
    private atentionAreasService: AtentionAreasService,
    private atenderTicketsService: AtenderTicketsService,
    private dialog: MatDialog
  ) {
    this.idEmployeeSelected = JSON.parse(localStorage.getItem('employeeInfo')).idEmpleado;
    this.formGroupTicket = new FormGroupTicket(this.formBuilder);
    this.profile = JSON.parse(localStorage.getItem('profile'));
    this.sedeSolicita = JSON.parse(localStorage.getItem('employeeInfo')).vchSede;
    this.getEmployeesList();
    // this.selected = 1;
    this.formGroupTicket.employeeField.setValue(this.idEmployeeSelected);
    this.changeName();
    this.getIncidencesList();
    this.getAtentionAreasEnable();

  }

  ngOnInit(): void { }

  private getIncidencesList() {
    let incidencesList: IncidencesModel[];
    this.incidencesService.getInstancesList().subscribe({
      next: (data: IncidencesModel[]) => { incidencesList = data['incidencias']; },
      error: (error) => { console.error('A ocurrido un error al obtener las Incidencias => ', error.message); },
      complete: () => { this.incidencesArray = incidencesList; }
    });
  }

  changeIncidence(evt) {
    this.getIncidencesDetailsListById(evt);
    this.enabledIncidenceDetails = false;
  }

  changeName() {
    this.enabledIncidence = false;
  }

  private getIncidencesDetailsListById(incidenceID: number) {
    let incidencesDetailsList: IncidencesDetailsModel[];
    this.incidencesService.getIncidencesDetailsListById(incidenceID).subscribe({
      next: (data: IncidencesDetailsModel[]) => { incidencesDetailsList = data['detalleIncidencias']; },
      error: (error) => { console.error('A ocurrido un error al obtener los Detalles de Incidencias', error.message); },
      complete: () => { this.incidencesDetailsArray = incidencesDetailsList; }
    });
  }

  changeIncidenceDetails() {
    this.enabledAtentionArea = false;
    this.enableOptionalMessage = false;
    this.enableOptionalImageButton = false;
    const optionText = this.incidencesDetailsArray.find(
      e => e.idDetalleIncidencia === this.formGroupTicket.incidenceDetailsField.value
    ).vchDescripcion;
    if (optionText === 'Otro') {
      this.enabledTextAreaDescription = false;
    } else {
      this.enabledTextAreaDescription = true;
    }
  }

  private getEmployeesList() {
    let employeesList: EmployeesModel[];
    this.employeesService.getEmployeesList().subscribe({
      next: (data: EmployeesModel[]) => {
        employeesList = data['empleados'][0];
      },
      error: (error) => {
        console.error('A ocurrido un error al obtener los registros de los Empleados');
      },
      complete: () => {
        this.employeesArray = Object.values(employeesList);
      }
    });
  }

  addTicketToList() {
    const incidenceDescription = this.incidencesArray[this.formGroupTicket.incidenceField.value - 1]['vchDescripcion'];
    const incidenceDetailDescription = this.incidencesDetailsArray.find(e => e.idDetalleIncidencia === this.formGroupTicket.incidenceDetailsField.value).vchDescripcion;
    const indexEmployeeDescription = this.employeesArray.findIndex(e => e.idEmpleado === this.formGroupTicket.employeeField.value);
    const employeeDescription = `${this.employeesArray[indexEmployeeDescription]['vchNombre']} ${this.employeesArray[indexEmployeeDescription]['vchApellidoPaterno']} ${this.employeesArray[indexEmployeeDescription]['vchApellidoMaterno']}`;
    const descriptionOther = this.formGroupTicket.descriptionIncidenceOtherField.value;
    const atentionAreaDescription = this.atentionAreasArray[this.formGroupTicket.atentionAreaField.value - 1]['vchDescripcion'];
    const additionalDescripcion = this.formGroupTicket.optionalDescriptionField.value;
    let nameImageSelected;

    // Arreglo de imagenes el cual se inserta junto con el mensaje
    if (this.fileImage) {
      nameImageSelected = this.fileImage.name;
      this.arrayImages.push({ ticket: 0, message: additionalDescripcion, image: this.fileImage });
    } else {
      this.arrayImages.push({ ticket: 0, message: additionalDescripcion, image: '' });
      nameImageSelected = '';
    }
    // --->

    const ticketItemFront = {
      vchIncidence: incidenceDescription,
      vchIncidenceDetail: incidenceDetailDescription,
      vchEmployee: employeeDescription,
      vchDescriptionOther: descriptionOther,
      vchAtentionAreaDescription: atentionAreaDescription,
      vchAdditionalDescripcion: additionalDescripcion,
      vchNameImageSelected: nameImageSelected,
      vchSedeSolicita: this.sedeSolicita
    };

    this.ticketsListFront.push(ticketItemFront);

    const ticketItemBack = {
      txtDescripcion: this.formGroupTicket.descriptionIncidenceOtherField.value,
      blnActivo: true,
      fkIdEmpleadoSolicita: this.formGroupTicket.employeeField.value,
      fkIdEstadoTicket: 1,
      idIncidencia: this.formGroupTicket.incidenceField.value,
      fkIdDetalleIncidencia: this.formGroupTicket.incidenceDetailsField.value,
      fkIdAreaAtencion: this.formGroupTicket.atentionAreaField.value,
      vchSedeSolicita: this.sedeSolicita
    };

    this.ticketsListBack.push(ticketItemBack);

    this.formGroupTicket.formGroupTicket.setValue({
      incidence: null,
      incidenceDetails: null,
      employee: null,
      descriptionIncidenceOther: null,
      atentionArea: null,
      optionalDescription: null
    });
    // console.info('Value del formGroup => ', this.formGroupTicket.formGroupTicket.value);
    this.enabledTextAreaDescription = true;
    this.enabledIncidenceDetails = true;
    this.enabledIncidence = true;
    this.enabledAtentionArea = true;
    this.enableOptionalImageButton = true;
    this.enableOptionalMessage = true;
    this.fileImage = null;
  }

  deleteTicket(index: number) {
    Swal.fire({
      title: '¿Deseas eliminar esta solicitud?',
      icon: 'question',
      showCancelButton: true,
      cancelButtonColor: '#f36b6b',
      cancelButtonText: 'CANCELAR',
      showConfirmButton: true,
      confirmButtonColor: '#80d65c',
      confirmButtonText: 'SI',
    }).then(resp => {
      if (resp.value) {
        Swal.fire({
          title: 'Ticket eliminado con éxito',
          icon: 'success',
        });
        this.ticketsListFront.splice(index, 1);
        this.ticketsListBack.splice(index, 1);
        this.arrayImages.splice(index, 1);
      } else {
        Swal.fire({
          title: 'El ticket no fue eliminado',
          icon: 'info',
        });
      }
    });
  }

  async sendTickets() {
    let index = 0;
    let ticketID = 0;
    this.chatClass = new ChatClass(0, this.messagesService, 0, 0, true);

    console.log('Array List Back => ', this.ticketsListBack);
    console.log('Array Images', this.arrayImages);

    for await (let ticket of this.ticketsListBack) {
      console.log('Ticket => ', ticket);

      this.ticketsService.postTickets(ticket).subscribe({
        next: (data: any) => {
          ticketID = data.nuevoTicket.idCreacionTicket;
          this.arrayImages[index].ticket = ticketID;
          /* if (this.arrayImages[index].image !== '') {
            this.saveImage(this.arrayImages[index].image, ticketID, this.arrayImages[index].message);
          } else if (this.arrayImages[index].message !== '' && this.arrayImages[index].message !== null) {
            this.postMessageChat(ticketID, this.arrayImages[index].message, '');
          } */
        },
        error: (error) => {
          console.error('Error => ', error);
          this.arrayImages = [];
        },
        complete: () => {

          this.ticketsListBack = [];
          this.ticketsListFront = [];
          // this.arrayImages.splice(index, 1);
          index++;
          Swal.fire({
            title: 'Los tickets han sido guardados correctamente',
            icon: 'success',
          });
        }
      });
    }

    setTimeout(() => {
      for (let index = 0; index < this.arrayImages.length; index++) {
        if (this.arrayImages[index].image !== '') {
          this.saveImage(this.arrayImages[index].image, this.arrayImages[index].ticket, this.arrayImages[index].message);
        } else if (this.arrayImages[index].message !== '' && this.arrayImages[index].message !== null) {
          this.postMessageChat(this.arrayImages[index].ticket, this.arrayImages[index].message, '');
        }
      }
    }, 3000)

    // console.log(this.arrayImages);

  }

  private saveImage(image: File, ticketID: number, message: string) {
    const imageName = `P${this.profile['profileid']}-D${Date.now()}`;
    this.messagesService.uploadImage(imageName, image, ticketID).subscribe({
      next: (data) => { },
      error: (error) => { console.error('Error al subir la imagen => ', error); },
      complete: () => {
        const urlImage = `chat${ticketID}/${imageName}.${image.type.split('/')[1]}`;
        this.postMessageChat(ticketID, message, urlImage);
      }
    });
  }

  // Método para guardar el mensaje
  postMessageChat(ticketID: number, message: string, imageName: string) {
    let receiver;
    this.atenderTicketsService.getAttendingEmployee(ticketID).subscribe({
      next: (data) => {
        const dataResponse: any = Object.values(data['attendingEmployee'][0]);
        if (dataResponse.length > 0) {
          receiver = dataResponse[0].fkIdEmpleadoAtiende;
        } else {
          receiver: 0
        }
      },
      error: (error) => { },
      complete: () => {
        this.chatClass = new ChatClass(ticketID, this.messagesService, receiver, 1, true);
        this.chatClass.setupSocketConnection();
        const dateNow = this.chatClass.getCurrentDate();
        const timeNow = this.chatClass.getCurrentTime();
        const imageurl = imageName;
        this.messagesArray.push({
          message: message,
          profileid: this.profile['profileid'],
          profilename: this.profile['profilename'],
          datesend: dateNow,
          timesend: timeNow,
          image: imageurl
        });
        this.chatClass.postMessagesChat(this.messagesArray);
        this.messagesArray = [];
      }
    });

  }

  private getAtentionAreasEnable() {
    let atentionAreas: AtentionAreasModel[];
    this.atentionAreasService.getAtentionAreasList().subscribe({
      next: (data) => {
        atentionAreas = data;
      },
      error: (error) => {
        console.error('A ocurrido un error al consultar las Áreas de Atención => ', error);
      },
      complete: () => {
        this.atentionAreasArray = atentionAreas;
      }
    });
  }

  // Métodos relacionados con la imagen
  url: any;
  searchImage() {
    const fileSearch = this.file.nativeElement;
    fileSearch.click();
  }

  onFileChange(event: any) {
    let reader = new FileReader();
    if (event.target.files && event.target.files.length) {
      const file = event.target.files[0];
      if (file.type.includes('image')) {
        this.fileImage = file;
        this.file.nativeElement.value = '';
        reader.readAsDataURL(file);
        reader.onload = (_event) => {
          this.url = reader.result;
        }
      }
    }
  }

  editTicket(index: number) {
    const itemEdit = [
      { ticket: this.ticketsListBack[index] },
      { message: this.arrayImages[index] },
      { front: this.ticketsListFront[index] }
    ];

    this.dialog.open(EditarTicketComponent, {
      panelClass: 'editarTicketModal',
      data: { itemTicket: itemEdit },
      disableClose: true,
    }).afterClosed().subscribe((data) => {
      if (data != null) {
        if (data.updated) {
          this.ticketsListBack[index] = data.ticketItemBack;
          this.ticketsListFront[index] = data.ticketItemFront;
          this.arrayImages[index] = data.message;
        } else if (data.deleted) {
          this.deleteTicket(index);
        }
      }
    });
  }

}
