import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { AtentionAreasModel } from '../../Models/AtentionAreas/AtentionAreas@Model';

@Injectable({providedIn: 'root'})
export class AtentionAreasService {

    constructor(
        private http: HttpClient
    ) { }
    
    getAtentionAreasList(): Observable<AtentionAreasModel[]> {
        return this.http.get<AtentionAreasModel[]>(`${environment.baseURL}/areas/enable`);
    }

}