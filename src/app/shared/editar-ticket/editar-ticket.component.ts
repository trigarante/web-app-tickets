import { Component, ElementRef, Inject, OnInit, ViewChild } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { AtentionAreasModel } from 'src/app/@core/Models/AtentionAreas/AtentionAreas@Model';
import { EmployeesModel } from 'src/app/@core/Models/Employees/Employees@Model';
import { IncidencesModel } from 'src/app/@core/Models/Incidences/Incidences@Model';
import { IncidencesDetailsModel } from 'src/app/@core/Models/Incidences/IncidencesDetails@Model';
import { AtentionAreasService } from 'src/app/@core/Services/AtentionAreas/atentionareas.service';
import { EmployeesService } from 'src/app/@core/Services/Employess/employees.service';
import { IncidensesService } from 'src/app/@core/Services/Incidences/incidences.service';

@Component({
    selector: 'app-editar-ticket',
    templateUrl: 'editar-ticket.component.html',
    styleUrls: ['editar-ticket.component.scss']
})

export class EditarTicketComponent implements OnInit {

    formGroupEdit: FormGroup;
    employeesArray: EmployeesModel[];
    incidencesArray: IncidencesModel[];
    incidencesDetailsArray: IncidencesDetailsModel[];
    atentionAreasArray: AtentionAreasModel[];

    enabledTextAreaDescription: boolean = true;

    infoEdit: any;

    @ViewChild('file') file: ElementRef;
    fileImage: File;
    arrayImages = [];

    url: any;

    constructor(
        private matDialogRef: MatDialogRef<EditarTicketComponent>,
        @Inject(MAT_DIALOG_DATA) public dataExternalDialog: any,
        private formBuilder: FormBuilder,
        private employeesService: EmployeesService,
        private incidencesService: IncidensesService,
        private atentionAreasService: AtentionAreasService
    ) {
        this.infoEdit = this.dataExternalDialog.itemTicket;
        this.startFormBuilder();
        this.getEmployeesList();
        this.getIncidencesList();
        this.changeIncidence(this.infoEdit[0].ticket.idIncidencia);
        this.optionalDescriptionField.setValue(this.infoEdit[1].message.message);
        this.getAtentionAreasEnable();
        if (this.infoEdit[1].message.image !== '') {
            let reader = new FileReader();
            this.fileImage = this.infoEdit[1].message.image;
            reader.readAsDataURL(this.infoEdit[1].message.image);
            reader.onload = (_event) => {
                this.url = reader.result;
            }
        }
    }

    ngOnInit() { }

    startFormBuilder() {
        this.formGroupEdit = this.formBuilder.group({
            employee: ['', [Validators.required]],
            incidence: ['', [Validators.required]],
            incidenceDetails: ['', [Validators.required]],
            atentionArea: ['', [Validators.required]],
            descriptionIncidenceOther: [null],
            optionalDescription: [null]
        });
    }

    private getEmployeesList() {
        let employeesList: EmployeesModel[];
        this.employeesService.getEmployeesList().subscribe({
            next: (data: EmployeesModel[]) => {
                employeesList = data['empleados'][0];
            },
            error: (error) => {
                console.error('A ocurrido un error al obtener los registros de los Empleados');
            },
            complete: () => {
                this.employeesArray = Object.values(employeesList);
                this.employeeField.setValue(this.infoEdit[0].ticket.fkIdEmpleadoSolicita);
            }
        });
    }

    private getIncidencesList() {
        let incidencesList: IncidencesModel[];
        this.incidencesService.getInstancesList().subscribe({
            next: (data: IncidencesModel[]) => { incidencesList = data['incidencias']; },
            error: (error) => { console.error('A ocurrido un error al obtener las Incidencias => ', error.message); },
            complete: () => {
                this.incidencesArray = incidencesList;
                this.incidenceField.setValue(this.infoEdit[0].ticket.idIncidencia);
            }
        });
    }

    changeIncidence(evt) {
        this.getIncidencesDetailsListById(evt);
    }

    private getIncidencesDetailsListById(incidenceID: number) {
        let incidencesDetailsList: IncidencesDetailsModel[];
        this.incidencesService.getIncidencesDetailsListById(incidenceID).subscribe({
            next: (data: IncidencesDetailsModel[]) => { incidencesDetailsList = data['detalleIncidencias']; },
            error: (error) => { console.error('A ocurrido un error al obtener los Detalles de Incidencias', error.message); },
            complete: () => {
                this.incidencesDetailsArray = incidencesDetailsList;
                this.incidenceDetailsField.setValue(this.infoEdit[0].ticket.fkIdDetalleIncidencia);
                this.changeIncidenceDetails();
            }
        });
    }

    changeIncidenceDetails() {
        try {
            const optionText = this.incidencesDetailsArray.find(
                e => e.idDetalleIncidencia === this.incidenceDetailsField.value
            ).vchDescripcion;
            if (optionText === 'Otro') {
                this.enabledTextAreaDescription = false;
                this.descriptionIncidenceOtherField.setValue(this.infoEdit[0].ticket.txtDescripcion)
            } else {
                this.enabledTextAreaDescription = true;
                this.infoEdit.txtDescripcion = null;
                this.descriptionIncidenceOtherField.setValue(null)
            }
        } catch (error) {

        }
    }

    private getAtentionAreasEnable() {
        let atentionAreas: AtentionAreasModel[];
        this.atentionAreasService.getAtentionAreasList().subscribe({
            next: (data) => {
                atentionAreas = data;
            },
            error: (error) => {
                console.error('A ocurrido un error al consultar las Áreas de Atención => ', error);
            },
            complete: () => {
                this.atentionAreasArray = atentionAreas;
                this.atentionAreaField.setValue(this.infoEdit[0].ticket.fkIdAreaAtencion);
            }
        });
    }

    cancelEdit() {
        this.matDialogRef.close(null);
    }

    deleteTicket() {
        this.matDialogRef.close({ deleted: true });
    }

    confirmUpdate() {

        const incidenceDescription = this.incidencesArray[this.incidenceField.value - 1]['vchDescripcion'];
        const incidenceDetailDescription = this.incidencesDetailsArray.find(e => e.idDetalleIncidencia === this.incidenceDetailsField.value).vchDescripcion;

        const indexEmployeeDescription = this.employeesArray.findIndex(e => e.idEmpleado === this.employeeField.value);

        const employeeDescription = `${this.employeesArray[indexEmployeeDescription]['vchNombre']} ${this.employeesArray[indexEmployeeDescription]['vchApellidoPaterno']} ${this.employeesArray[indexEmployeeDescription]['vchApellidoMaterno']}`;
        const descriptionOther = this.descriptionIncidenceOtherField.value;
        const atentionAreaDescription = this.atentionAreasArray[this.atentionAreaField.value - 1]['vchDescripcion'];
        const additionalDescripcion = this.optionalDescriptionField.value;
        let nameImageSelected: string;

        if (this.fileImage) {
            nameImageSelected = this.fileImage.name;
            this.arrayImages.push({ message: additionalDescripcion, image: this.fileImage });
        } else {
            this.arrayImages.push({ message: additionalDescripcion, image: '' });
            nameImageSelected = '';
        }

        const ticketItemBack = {
            txtDescripcion: this.descriptionIncidenceOtherField.value,
            blnActivo: true,
            fkIdEmpleadoSolicita: this.employeeField.value,
            fkIdEstadoTicket: 1,
            idIncidencia: this.incidenceField.value,
            fkIdDetalleIncidencia: this.incidenceDetailsField.value,
            fkIdAreaAtencion: this.atentionAreaField.value,
            vchSedeSolicita: this.infoEdit[0].ticket.vchSedeSolicita
        };

        const ticketItemFront = {
            vchIncidence: incidenceDescription,
            vchIncidenceDetail: incidenceDetailDescription,
            vchEmployee: employeeDescription,
            vchDescriptionOther: descriptionOther,
            vchAtentionAreaDescription: atentionAreaDescription,
            vchAdditionalDescripcion: additionalDescripcion,
            vchNameImageSelected: nameImageSelected,
            vchSedeSolicita: this.infoEdit[0].ticket.vchSedeSolicita
        };

        const message = { message: this.optionalDescriptionField.value, image: this.fileImage ? this.fileImage : '' };

        this.matDialogRef.close({ ticketItemBack, message, ticketItemFront, updated: true });
    }

    searchImage() {
        const fileSearch = this.file.nativeElement;
        fileSearch.click();
    }

    onFileChange(event: any) {
        let reader = new FileReader();
        if (event.target.files && event.target.files.length) {
            const file = event.target.files[0];
            if (file.type.includes('image')) {
                this.fileImage = file;
                this.file.nativeElement.value = '';
                reader.readAsDataURL(file);
                reader.onload = (_event) => {
                    this.url = reader.result;
                }
            }
        }
    }

    deleteImage() {
        this.file.nativeElement.value = '';
        this.fileImage = null;
    }

    // Propiedades
    get employeeField(): AbstractControl {
        return this.formGroupEdit.get('employee');
    }

    get incidenceField(): AbstractControl {
        return this.formGroupEdit.get('incidence');
    }

    get incidenceDetailsField(): AbstractControl {
        return this.formGroupEdit.get('incidenceDetails');
    }

    get descriptionIncidenceOtherField(): AbstractControl {
        return this.formGroupEdit.get('descriptionIncidenceOther');
    }

    get optionalDescriptionField(): AbstractControl {
        return this.formGroupEdit.get('optionalDescription');
    }

    get atentionAreaField(): AbstractControl {
        return this.formGroupEdit.get('atentionArea');
    }

}