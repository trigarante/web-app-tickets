import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedRoutingModule } from './shared-routing.module';
import { MessagesComponent } from './messages/messages.component';
import { ThemeModule } from '../@theme/@theme.module';
import { MatIconModule } from '@angular/material/icon';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatMenuModule } from '@angular/material/menu';
import { ImagesCarrouselChatComponent } from './images-carrousel-chat/images-carrousel-chat.component';
import { MatCarouselModule } from '@ngbmodule/material-carousel';
import { SlickCarouselModule } from 'ngx-slick-carousel';
import { EditarTicketComponent } from './editar-ticket/editar-ticket.component';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatSelectModule } from '@angular/material/select';
import { MatInputModule } from '@angular/material/input';
import { MatListModule } from '@angular/material/list'
import { MatTabsModule } from '@angular/material/tabs';
import { PermissionsModalComponent } from './permissions/permissions-modal.component';
import { MatDialogModule } from '@angular/material/dialog';

@NgModule({
  declarations: [
    MessagesComponent,
    ImagesCarrouselChatComponent,
    EditarTicketComponent,
    PermissionsModalComponent
  ],
  imports: [
    CommonModule,
    SharedRoutingModule,
    ThemeModule,
    MatIconModule,
    MatButtonModule,
    MatExpansionModule,
    FormsModule,
    MatMenuModule,
    MatCarouselModule.forRoot(),
    SlickCarouselModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatSelectModule,
    MatInputModule,
    MatListModule,
    MatTabsModule,
    MatDialogModule
  ],
  exports: [
    MessagesComponent,
    ImagesCarrouselChatComponent
  ]
})
export class SharedModule { }
