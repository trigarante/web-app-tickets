export interface MenusModel {
    vchIdentificador: string;
    vchIcono: string;
    vchNombre: string;
    vchRuta: string;
}