import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LevantarTicketRoutingModule } from './levantar-ticket-routing.module';
import { LevantarTicketComponent } from './levantar-ticket/levantar-ticket.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatInputModule } from '@angular/material/input';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatSelectModule } from '@angular/material/select';
import { MatExpansionModule } from '@angular/material/expansion';
import { IncidensesService } from 'src/app/@core/Services/Incidences/incidences.service';
import { EmployeesService } from 'src/app/@core/Services/Employess/employees.service';
import {MatButtonModule} from '@angular/material/button';
import { TicketsService } from 'src/app/@core/Services/Tikcets/tickets.service';
import {MatIconModule} from '@angular/material/icon';
import {MatTooltipModule} from '@angular/material/tooltip';
import { MatDialogModule } from '@angular/material/dialog';


@NgModule({
  declarations: [
    LevantarTicketComponent
  ],
  imports: [
    CommonModule,
    LevantarTicketRoutingModule,
    FormsModule,
    MatInputModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatSelectModule,
    MatExpansionModule,
    MatButtonModule,
    MatIconModule,
    MatTooltipModule,
    MatDialogModule
  ],
  providers: [
    IncidensesService,
    EmployeesService,
    TicketsService
  ]
})
export class LevantarTicketModule { }
