import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { MenusAddedModel } from '../../Models/Menus/MenusAdded@Model';
import { MenusNotAddedModel } from '../../Models/Menus/MenusNotAdded@Model';

@Injectable({ providedIn: 'root' })
export class MenusService {

    constructor(
        private http: HttpClient
    ) { }

    getmenusAdded(employeeID: number): Observable<MenusAddedModel[]> {
        return this.http.get<MenusAddedModel[]>(`${environment.baseURL}/menus/added/${employeeID}`);
    }

    getMenusNotAdded(employeeID: number): Observable<MenusNotAddedModel[]> {
        return this.http.get<MenusNotAddedModel[]>(`${environment.baseURL}/menus/not-added/${employeeID}`);
    }

    putMenusStatusActive(arrayData: any) {
        const employeeID = arrayData.idEmpleado;
        const datamenu = { idMenu: arrayData.idMenu, status: arrayData.status };
        return this.http.put(`${environment.baseURL}/menus/update-menus/${employeeID}`, datamenu);
    }

    postMenusToEmployee(arrayData: any) {
        const employeeID = arrayData.idEmpleado;
        const datamenu = { idMenu: arrayData.idMenu, status: arrayData.status };
        return this.http.post(`${environment.baseURL}/menus/insert-menus/${employeeID}`, datamenu);
    }

}