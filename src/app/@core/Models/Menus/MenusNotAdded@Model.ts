export interface MenusNotAddedModel {
    idMenu: number;
    vchNombre: string;
    vchIcono: string;
    vchRuta: string;
}