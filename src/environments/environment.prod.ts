export const environment = {
  production: true,
  baseURL: 'https://servicios.ticketseguimiento.com/api',
  SOCKET_ENDPOINT: 'https://servicios.ticketseguimiento.com'
};
