import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { EmployeesModel } from '../../Models/Employees/Employees@Model';
import { EmployeesPermission } from '../../Models/Employees/EmployeesPermissions@Model';
import { MenusModel } from '../../Models/Menus/Menus@Model';

@Injectable({ providedIn: 'root' })
export class EmployeesService {

    constructor(
        private http: HttpClient
    ) { }

    getEmployeesList(): Observable<EmployeesModel[]> {
        return this.http.get<EmployeesModel[]>(`${environment.baseURL}/employees`);
    }

    getEmployeeByEmail() {
        const storage = JSON.parse(localStorage.getItem('profile'));
        const email = storage['profileemail'];
        return this.http.get(`${environment.baseURL}/employees/by-email/${email}`);
    }

    getEmployeeMenusById(): Observable<MenusModel[]> {
        const employeeInfo = JSON.parse(localStorage.getItem('employeeInfo'));
        const idEmpleado = employeeInfo['idEmpleado'];
        return this.http.get<MenusModel[]>(`${environment.baseURL}/employees/menus/${idEmpleado}`);
    }

    getEmployeesSupport(): Observable<EmployeesPermission[]> {
        const employeeInfo = JSON.parse(localStorage.getItem('employeeInfo'));
        const sede = employeeInfo['vchSede'];
        return this.http.get<EmployeesPermission[]>(`${environment.baseURL}/permissions/employees-support/${sede}`);
    }

    getEmployeeAttendingTicketById(ticketID: number) {
        return this.http.get<EmployeesPermission[]>(`${environment.baseURL}/employees/ticket-employee/${ticketID}`);
    }

    getEmployeesByAtentionAreaID(atentionAreaID: number) {
        const employeeInfo = JSON.parse(localStorage.getItem('employeeInfo'));
        const sede = employeeInfo['vchSede'];
        return this.http.get<EmployeesPermission[]>(`${environment.baseURL}/employees/by-atention-area/${atentionAreaID}/${sede}`);
    }

    getEmployeesByUserTypeID(userTypeID: number) {
        const employeeInfo = JSON.parse(localStorage.getItem('employeeInfo'));
        const sede = employeeInfo['vchSede'];
        return this.http.get(`${environment.baseURL}/employees/get-employees-by-user-type/${userTypeID}/${sede}`);
    }

    putEmployeeStatausAccount(employeeID: number, status: boolean) {
        const array = { status };
        return this.http.put(`${environment.baseURL}/employees/update-status-account/${employeeID}`, array);
    }

    postNewEmployee(email: string, employeeID: number) {
        const data = {
            email: email,
            idempleado: employeeID
        };

        return this.http.post(`${environment.baseURL}/employees`, data);
    }

    putUserTypeEmployee(employeeID: number, userTypeID: number, areaAtencion = null) {
        const data = {
            tipousuario: userTypeID,
            areaAtencion: areaAtencion
        };

        return this.http.put(`${environment.baseURL}/employees/update-user-type-id/${employeeID}`, data);
    }

}

