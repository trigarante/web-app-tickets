import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator, MatPaginatorIntl } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { TicketsService } from 'src/app/@core/Services/Tikcets/tickets.service';
import { AssignTicketsModalComponent } from '../assign-tickets-modal/assign-tickets-modal.component';
import { ModalDetailsComponent } from '../modal-details/modal-details.component';

@Component({
    selector: 'app-table-tickets-not-assinged',
    templateUrl: 'table-tickets-not-assigned.component.html',
    styleUrls: ['table-tickets-not-assigned.component.scss']
})

export class TableTicktesNotAssignedComponent implements OnInit {

    // Para los tickets no asignados
    dataSourceNotAssigned: MatTableDataSource<any>;
    displayedColumnsNotAssigned = ['ticketID', 'dtFechaSolicitud', 'vchEmpleadoSolicita', 'vchIncidencia', 'vchAreaAtencion', 'vchSede', 'detalles', 'ajustes'];
    @ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChild(MatSort) sort: MatSort;

    ticketsNotAssigned = [];

    sede: string;

    loaderActivated: boolean = true;

    constructor(
        private pagin: MatPaginatorIntl,
        private ticketsService: TicketsService,
        private dialog: MatDialog
    ) {
        this.sede = JSON.parse(localStorage.getItem('employeeInfo')).vchSede;
        this.getTickesNotAssigned();
    }

    ngOnInit() { }

    applyFilterNotAssigned(evt) {
        const searchValue = (evt.target as HTMLInputElement).value;
        this.dataSourceNotAssigned.filter = searchValue.trim().toLocaleLowerCase();
        if (this.dataSourceNotAssigned.paginator) {
            this.dataSourceNotAssigned.paginator.firstPage();
        }
    }

    getTickesNotAssigned() {
        this.ticketsService.getTicketsNotAttending(this.sede).subscribe({
            next: (data) => {
                this.ticketsNotAssigned = Object.values(data['ticketsNotAssgined'][0]);
            },
            error: (error) => { console.error('Error al consultar los tickets no asignados => ', error); },
            complete: () => {
                this.loaderActivated = false;
                this.dataSourceNotAssigned = new MatTableDataSource(this.ticketsNotAssigned);
                this.dataSourceNotAssigned.sort = this.sort;
                this.dataSourceNotAssigned.paginator = this.paginator;
            }
        })
    }

    openDetailModal(item: any, exists: boolean) {
        this.dialog.open(ModalDetailsComponent, {
            data: {
                itemTicket: item,
                ticketExistente: exists
            },
            disableClose: true,
            panelClass: 'customModalAtencionTickets',
        });
    }

    openAssignTicketModal(ticketID: number, idAreaAtencion: number) {
        this.dialog.open(AssignTicketsModalComponent, {
            data: {
                ticketID,
                idAreaAtencion
            },
            panelClass: 'editarTicketModal',
            width: '500px',
            height: '350px',
            disableClose: true
        }).afterClosed().subscribe(data => {
            if (data.assigned) {
                const index = this.ticketsNotAssigned.findIndex(e => e.idCreacionTicket === ticketID);
                this.ticketsNotAssigned.splice(index, 1);
                this.dataSourceNotAssigned = new MatTableDataSource(this.ticketsNotAssigned);
                this.dataSourceNotAssigned.sort = this.sort;
                this.dataSourceNotAssigned.paginator = this.paginator;
            }
        });
    }

}