import { AbstractControl, FormBuilder, FormGroup, Validators } from "@angular/forms";

export class FormGroupTicket {

    private formGroup: FormGroup;

    constructor(
        private formBuilder: FormBuilder
    ) {
        this.startFormBuild();
    }

    private startFormBuild() {
        this.formGroup = this.formBuilder.group({
            incidence: [null, [Validators.required]],
            incidenceDetails: [null, [Validators.required]],
            employee: [null, [Validators.required]],
            atentionArea: [null, Validators.required],
            descriptionIncidenceOther: [null],
            optionalDescription: [''],
        });
    }

    //#region Propiedades get para acceder a las propiedades del formGroup
    get incidenceField(): AbstractControl {
        return this.formGroup.get('incidence');
    }

    get incidenceDetailsField(): AbstractControl {
        return this.formGroup.get('incidenceDetails');
    }

    get employeeField(): AbstractControl {
        return this.formGroup.get('employee');
    }

    get atentionAreaField(): AbstractControl {
        return this.formGroup.get('atentionArea');
    }

    get descriptionIncidenceOtherField(): AbstractControl {
        return this.formGroup.get('descriptionIncidenceOther');
    }

    get optionalDescriptionField(): AbstractControl {
        return this.formGroup.get('optionalDescription');
    }
    //#endregion

    // Propiedad para acceder al formGroup

    get formGroupTicket(): FormGroup {
        return this.formGroup;
    }

}
