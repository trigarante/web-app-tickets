import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ModalComponent } from './modal-component/modal/modal.component';
import { MyTicketsRoutingModule } from './my-tickets-routing.module';
import { MyTicketsComponent } from './my-tickets-component/my-tickets/my-tickets.component';
import { MatFormFieldModule } from '@angular/material/form-field';
import {MatTableModule} from '@angular/material/table';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatInputModule } from '@angular/material/input';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatSelectModule } from '@angular/material/select';
import { MatButtonModule } from '@angular/material/button';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatIconModule } from '@angular/material/icon';
import {MatDialogModule} from '@angular/material/dialog';
import {MatSortModule} from "@angular/material/sort";
import {MatTooltipModule} from '@angular/material/tooltip';

@NgModule({
    declarations: [
        MyTicketsComponent
    ],
    imports: [
        CommonModule,
        MyTicketsRoutingModule,
        MatFormFieldModule,
        MatTableModule,
        MatPaginatorModule,
        MatInputModule,
        ReactiveFormsModule,
        FormsModule,
        MatSelectModule,
        MatButtonModule,
        MatExpansionModule,
        MatIconModule,
        MatDialogModule,
        MatSortModule,
        MatTooltipModule
    ]
})
export class MyTicketsModule { }
