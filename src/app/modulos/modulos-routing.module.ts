import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {ModulosComponent} from './modulos.component';
import {AuthGuard} from '../@core/Guards/auth.guard';

const routes: Routes = [{
  path: '',
  component: ModulosComponent,
  children: [
    {
      path: 'dashboard',
      // canActivate: [AuthGuard],
      // canLoad: [AuthGuard],
      loadChildren: () => import('./dashboard/dashboard.module').then(m => m.DashboardModule)
    },
    {
      path: 'levantar-ticket',
      loadChildren: () => import('./levantar-ticket/levantar-ticket.module').then(m => m.LevantarTicketModule),
    },
    {
      path: 'atencion-ticket',
      loadChildren: () => import('./atencion-tickets/atencion-tickets.module').then(m => m.AtencionTicketsModule),
    },
    {
      path: 'my-tickets',
      loadChildren: () => import('./my-tickets/my-tickets.module').then(m => m.MyTicketsModule)
    },
    {
      path: 'admin',
      loadChildren: () => import('./admin/admin.module').then(m => m.AdminModule)
    },
  ]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ModulosRoutingModule {}
