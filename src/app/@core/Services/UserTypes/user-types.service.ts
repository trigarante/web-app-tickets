import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({ providedIn: 'root' })
export class UserTypesService {

    constructor(
        private http: HttpClient
    ) { }

    getAllUserTypes(): Observable<[]> {
        return this.http.get<[]>(`${environment.baseURL}/user-types`);
    }

}