export interface EmployeesModel {
    idEmpleado: number;
    vchNombre: string;
    vchApellidoPaterno: string;
    vchApellidoMaterno: string;
}