import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AtencionTicketsRoutingModule } from './atencion-tickets-routing.module';
import { AtencionTicketsComponent } from './atencion-tickets/atencion-tickets.component';
import { MatTableModule } from '@angular/material/table';
import { MatSortModule } from '@angular/material/sort';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatCardModule } from '@angular/material/card';
import { MatIconModule } from '@angular/material/icon';
import { MatTooltipModule } from '@angular/material/tooltip';
import { DetallesTicketAtencionModalComponent } from './modals/detalles-ticket-atencion-modal/detalles-ticket-atencion-modal.component';
import { MatDialogModule } from '@angular/material/dialog';
import { MatButtonModule } from '@angular/material/button';
import { AtenderTicketModalComponent } from './modals/atender-ticket-modal/atender-ticket-modal.component';
import { SharedModule } from 'src/app/shared/shared.module';


@NgModule({
  declarations: [
    AtencionTicketsComponent,
    DetallesTicketAtencionModalComponent,
    AtenderTicketModalComponent
  ],
  imports: [
    CommonModule,
    AtencionTicketsRoutingModule,
    MatTableModule,
    MatSortModule,
    MatPaginatorModule,
    MatFormFieldModule,
    MatInputModule,
    MatCardModule,
    MatIconModule,
    MatTooltipModule,
    MatDialogModule,
    MatButtonModule,
    SharedModule
  ]
})
export class AtencionTicketsModule { }
