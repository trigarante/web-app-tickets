export interface MenusAddedModel {
    idMenu: number;
    vchNombre: string;
    vchIcono: string;
    vchRuta: string;
    blnActivo: boolean;
}