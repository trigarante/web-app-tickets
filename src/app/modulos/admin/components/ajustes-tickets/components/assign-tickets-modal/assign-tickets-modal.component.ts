import { Component, Inject, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { AtentionAreasModel } from 'src/app/@core/Models/AtentionAreas/AtentionAreas@Model';
import { AtenderTicketsService } from 'src/app/@core/Services/Atender-Tickets/atender-tickets.service';
import { AtentionAreasService } from 'src/app/@core/Services/AtentionAreas/atentionareas.service';
import { EmployeesService } from 'src/app/@core/Services/Employess/employees.service';

import Swal from 'sweetalert2';

@Component({
    selector: 'app-assign-tickets-modal',
    templateUrl: 'assign-tickets-modal.component.html',
    styleUrls: ['assign-tickets-modal.component.scss']
})

export class AssignTicketsModalComponent implements OnInit {

    ticketID: number;
    areaAtencionID: number;

    employeesAttending = [];

    atentionAreasArray: AtentionAreasModel[];

    formGroup: FormGroup;

    constructor(
        private dialogRef: MatDialogRef<AssignTicketsModalComponent>,
        @Inject(MAT_DIALOG_DATA) public dataExternalDialog: any,
        private employeesService: EmployeesService,
        private formBuilder: FormBuilder,
        private atenderTicketsService: AtenderTicketsService,
        private atentionAreasService: AtentionAreasService
    ) {
        this.ticketID = this.dataExternalDialog.ticketID;
        this.areaAtencionID = this.dataExternalDialog.idAreaAtencion;
        this.startFormBuild();
        this.getEmployeesAttendingTickets();
        this.loadAttentionArea();
    }

    ngOnInit() { }

    startFormBuild() {
        this.formGroup = this.formBuilder.group({
            employeeAttending: [null, [Validators.required]],
            atentionArea: [null, [Validators.required]]
        });
    }

    closeDialog() {
        this.dialogRef.close({ assigned: false });
    }

    getEmployeesAttendingTickets() {
        let employeesResponse = [];
        this.employeesService.getEmployeesByAtentionAreaID(this.areaAtencionID).subscribe({
            next: (response) => { employeesResponse = Object.values(response['atentionAreaEmployees'][0]); },
            error: (error) => { console.error('Error al consultar los asesores de tickets => ', error); },
            complete: () => { this.employeesAttending = employeesResponse; }
        });
    }

    assignEmployeeToTheTicket() {

        const info = {
            ticketID: this.ticketID,
            employeeID: this.employeeAttendingField.value
        };
        this.atenderTicketsService.postEmployeeTicket(info).subscribe({
            next: (response) => { },
            error: (error) => {
                console.error('Error al insertar el registro en la base de datos => ', error);
                Swal.fire({
                    title: 'Error',
                    text: `A ocurrido un error al guardar el registro en la base de datos`,
                    icon: 'error',
                });
            },
            complete: () => {
                Swal.fire({
                    title: 'Asesor asignado',
                    text: `El asesor ha sido asignado al ticket #${this.ticketID}`,
                    icon: 'success',
                });
                this.dialogRef.close({ assigned: true });
            }
        });
    }

    loadAttentionArea() {
        let atentionAreas: AtentionAreasModel[];
        this.atentionAreasService.getAtentionAreasList().subscribe({
            next: (data) => {
                atentionAreas = data;
            },
            error: (error) => {
                console.error('A ocurrido un error al consultar las Áreas de Atención => ', error);
            },
            complete: () => {
                this.atentionAreasArray = atentionAreas;
                this.atentionAreaField.setValue(this.areaAtencionID);
            }
        });
    }

    changeArea() {
        this.areaAtencionID = this.atentionAreaField.value;
        this.getEmployeesAttendingTickets();
    }

    // Propiedades get para acceder a los valores del formGroup
    get employeeAttendingField(): AbstractControl {
        return this.formGroup.get('employeeAttending');
    }

    get atentionAreaField(): AbstractControl {
        return this.formGroup.get('atentionArea');
    }

}