export interface AtenderTickets {
  fkIdCreacionTicket: number;
  fechaSolicitud: Date;
  txtDescripcion: string;
  vchNombre: string;
  vchApellidoPaterno: string;
  vchApellidoMaterno: string;
  idEstadoTicket: number;
  vchDescripcionEstadoTickets: string;
  vchDescripcionDetalleIncidencias: string;
  vchDescripcionIncidencias: string;
  vchSede: string;
}
