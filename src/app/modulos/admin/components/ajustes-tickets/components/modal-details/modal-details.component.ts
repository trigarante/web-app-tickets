import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { EmployeesService } from 'src/app/@core/Services/Employess/employees.service';

@Component({
    selector: 'app-modal-details-ajustes-tickets',
    templateUrl: 'modal-details.component.html',
    styleUrls: ['modal-details.component.scss']
})

export class ModalDetailsComponent implements OnInit {

    dataTicket: any;
    ticketExists: boolean;
    employeeNameAttendingTicket: string = 'Sin atender';

    constructor(
        private matDialogRef: MatDialogRef<ModalDetailsComponent>,
        @Inject(MAT_DIALOG_DATA) public dataExternalDialog: any,
        private employeesService: EmployeesService
    ) {
        this.dataTicket = this.dataExternalDialog.itemTicket;
        this.ticketExists = this.dataExternalDialog.ticketExistente;
        if (this.ticketExists === true) {
            this.getEmployeeAttendingTicket();
        }
    }

    ngOnInit() { }

    closeModal() {
        this.matDialogRef.close();
    }

    getEmployeeAttendingTicket() {
        const ticketID = this.dataTicket.fkIdCreacionTicket;
        let dataEmployeeAttending;
        this.employeesService.getEmployeeAttendingTicketById(ticketID).subscribe({
            next: (result) => {
                dataEmployeeAttending = Object.values(result['employeeTicket'][0]);
            },
            error: (error) => {
                console.error('Error al consultar el empleado que atiende este ticket => ', error);
            },
            complete: () => {
                this.employeeNameAttendingTicket = `${dataEmployeeAttending[0].vchNombre} ${dataEmployeeAttending[0].vchApellidoPaterno} ${dataEmployeeAttending[0].vchApellidoMaterno}`;
            }
        });
    }

}