import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { AtenderTicketsService } from '../../../../@core/Services/Atender-Tickets/atender-tickets.service';

@Component({
  selector: 'app-atender-ticket-modal',
  templateUrl: './atender-ticket-modal.component.html',
  styleUrls: ['./atender-ticket-modal.component.scss']
})
export class AtenderTicketModalComponent implements OnInit {
  dataTicket: any;

  newEstadoTicket: number;
  stateDescription = ['si deseas atenderlo solo da click en Atender Ticket',
    'si deseas finalizar la atención de este ticket solo da click en Cerrar Ticket',
    'lo que significa que ya ha sido atendido                   '
  ];

  changedStatus: boolean = false;


  constructor(
    private matDialogRef: MatDialogRef<AtenderTicketModalComponent>,
    @Inject(MAT_DIALOG_DATA) public dataExternalDialog: any,
    private atenderTicketService: AtenderTicketsService,
  ) {
    this.dataTicket = this.dataExternalDialog.itemTicket;
  }

  ngOnInit(): void {
    this.dataExternalDialog = 2;
  }



  updateTicketState(creacionTicket, estadoTicket) {
    if (estadoTicket < 3) {
      this.newEstadoTicket = estadoTicket + 1;

      this.atenderTicketService.putEstadoTicket(creacionTicket, this.newEstadoTicket).subscribe({
        next: (res) => {
          this.changedStatus = true;
        },
        error: (error) => {
          this.newEstadoTicket = estadoTicket;
          this.changedStatus = false;
          console.error('Error al actualizar registro => ', error);
        },
        complete: () => {
          this.closeModal(creacionTicket);
        }
      });
    }
  }

  closeModal(idTicket): any {
    const estadosTickets = ['Pendiente', 'En Proceso', 'Cerrado']

    this.matDialogRef.close({ idTicket: idTicket, estadoTicket: this.newEstadoTicket, descripcionEstadoTicket: estadosTickets[this.newEstadoTicket - 1], changedStatus: this.changedStatus });
  }

}
