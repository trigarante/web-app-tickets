import { Component, Inject, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { AtentionAreasModel } from 'src/app/@core/Models/AtentionAreas/AtentionAreas@Model';
import { AtentionAreasService } from 'src/app/@core/Services/AtentionAreas/atentionareas.service';
import { EmployeesService } from 'src/app/@core/Services/Employess/employees.service';
import { UserTypesService } from 'src/app/@core/Services/UserTypes/user-types.service';
import { PermissionsModalComponent } from 'src/app/shared/permissions/permissions-modal.component';

import Swal from 'sweetalert2';

@Component({
    selector: 'app-manage-profiles',
    templateUrl: 'manage-profiles.component.html',
    styleUrls: ['manage-profiles.component.scss']
})

export class ManageProfilesComponent implements OnInit {

    formGroup: FormGroup;

    // Variables a asignar su valor que vienen desde el componete padre donde se llama
    empleadoID: number;
    tipoUsuarioID: number;
    areaAtencionID: number;

    userTypes = [];
    atentionAreasArray: AtentionAreasModel[];

    // Variable para habilitar/deshabilitar el botó de guardar los cambios
    enableButtonSaveChanges: boolean = false;

    constructor(
        private matDialogRef: MatDialogRef<ManageProfilesComponent>,
        @Inject(MAT_DIALOG_DATA) public dataExternalDialog: any,
        private formBuilder: FormBuilder,
        private userTypesService: UserTypesService,
        private dialog: MatDialog,
        private atentionAreasService: AtentionAreasService,
        private employeesService: EmployeesService
    ) {
        this.empleadoID = dataExternalDialog.empleadoID;
        this.tipoUsuarioID = dataExternalDialog.tipoUsuarioID;
        this.areaAtencionID = dataExternalDialog.areaAtencionID;

        this.getuserTypes();
        this.startFormBuild();
    }

    ngOnInit() { }

    closeDialog() {
        this.matDialogRef.close({ changes: false });
    }

    saveChanges() {

        if (this.formGroup.valid) {
            if (this.userTypeField.value === 3) {
                if (this.atentionAreaField.value !== null) {
                    this.saveMethod();
                } else {
                    Swal.fire({
                        title: 'Asignar Área de Atención',
                        text: 'Debe asignar una Área de Atención a este empleado',
                        icon: 'warning'
                    });
                }
            } else {
                this.saveMethod();
            }
        }
    }

    saveMethod() {

        this.employeesService.putUserTypeEmployee(this.empleadoID, this.userTypeField.value, this.atentionAreaField.value).subscribe({
            next: (response) => {
                console.log('Respuesta del servidor => ', response);
            },
            error: (error) => {
                console.error('Error al actualizar el tipo de usuario => ', error);
            },
            complete: () => {
                Swal.fire({
                    title: '¿Quiere realizar ajustes en los menús asignados a este empleado?',
                    showCancelButton: true,
                    confirmButtonText: 'Sí, continuar',
                    cancelButtonText: 'No, cerrar',
                    cancelButtonColor: 'red',
                    confirmButtonColor: 'green',
                    icon: 'question'
                }).then((result) => {
                    if (result.isConfirmed) {
                        this.permissionsSettings();
                    } else if (result.isDismissed) {
                        Swal.fire({
                            title: 'Cambios guardados',
                            text: 'El Tipo de Usuario ha sido actualizado',
                            icon: 'info'
                        });
                    }
                    this.matDialogRef.close({ changes: true });
                });
            }
        });
    }

    permissionsSettings() {
        const employeeID: number = this.empleadoID;
        this.dialog.open(PermissionsModalComponent, {
            data: {
                employeeID
            },
            panelClass: 'editarTicketModal',
            width: '500px',
            height: '550px',
            disableClose: true
        });
    }

    private getuserTypes() {
        let userTypesResponse = [];
        this.userTypesService.getAllUserTypes().subscribe({
            next: (response) => {
                userTypesResponse = response['tipoUsuarios'];
            },
            error: (error) => {
                console.error('Error al consultar los tipos de usuarios');
            },
            complete: () => {
                this.userTypes = userTypesResponse;
                this.userTypeField.setValue(this.tipoUsuarioID);
                this.getAreaAtention();
            }
        })
    }

    changeUserType() {
        if (this.tipoUsuarioID === this.userTypeField.value) {
            this.enableButtonSaveChanges = false;
        } else {
            this.enableButtonSaveChanges = true;
        }

        if (this.userTypeField.value === 3) {
            this.getAreaAtention();
        } else {
            this.atentionAreaField.setValue(null);
        }
    }

    private getAreaAtention() {
        let atentionAreas: AtentionAreasModel[];
        this.atentionAreasService.getAtentionAreasList().subscribe({
            next: (data) => {
                atentionAreas = data;
            },
            error: (error) => {
                console.error('A ocurrido un error al consultar las Áreas de Atención => ', error);
            },
            complete: () => {
                this.atentionAreasArray = atentionAreas;
                if (this.tipoUsuarioID !== null) {
                    this.atentionAreaField.setValue(this.areaAtencionID);
                }
            }
        });
    }

    changeAtentionArea() {
        if (this.areaAtencionID === this.atentionAreaField.value) {
            this.enableButtonSaveChanges = false;
        } else {
            this.enableButtonSaveChanges = true;
        }
    }

    // Método para construir el formGroup
    startFormBuild() {
        this.formGroup = this.formBuilder.group({
            userType: [null, [Validators.required]],
            atentionArea: [null]
        });
    }

    // Propiedades get para acceder a las propiedades del formGroup
    get userTypeField(): AbstractControl {
        return this.formGroup.get('userType');
    }

    get atentionAreaField(): AbstractControl {
        return this.formGroup.get('atentionArea');
    }

}