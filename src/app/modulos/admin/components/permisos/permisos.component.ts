import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator, MatPaginatorIntl } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { EmployeesPermission } from 'src/app/@core/Models/Employees/EmployeesPermissions@Model';
import { EmployeesService } from 'src/app/@core/Services/Employess/employees.service';
import { PermissionsModalComponent } from 'src/app/shared/permissions/permissions-modal.component';

@Component({
  selector: 'app-permisos',
  templateUrl: './permisos.component.html',
  styleUrls: ['./permisos.component.scss']
})
export class PermisosComponent implements OnInit {

  dataSource: MatTableDataSource<EmployeesPermission>;
  displayedColumns: string[] = ['numEmpleado', 'vchNombre', 'vchSede', 'vchTipoUsuario', 'vchAreaAtencionDescripcion', 'permissions'];
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(
    private pagin: MatPaginatorIntl,
    private employeesService: EmployeesService,
    private dialog: MatDialog
  ) {
    this.pagin.itemsPerPageLabel = 'Registros por página';
    this.pagin.firstPageLabel = 'Primera página';
    this.pagin.lastPageLabel = 'Última página';
    this.pagin.nextPageLabel = 'Página siguiente';
    this.pagin.previousPageLabel = 'Página anterior';
    this.getEmployeesPermissions();
    this.dataSource = new MatTableDataSource();
  }

  ngOnInit(): void {
  }

   applyFilter(evt: Event) {
    const searchValue = (evt.target as HTMLInputElement).value;
    this.dataSource.filter = searchValue.trim().toLocaleLowerCase();
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  getEmployeesPermissions() {
    let employeesPermissionsResponse;
    this.employeesService.getEmployeesSupport().subscribe({
      next: (response) => {
        employeesPermissionsResponse = Object.values(response['employeesSupport'][0]);
      },
      error: (error) => {
        console.error('Error al consumir el servicio => ', error);
      },
      complete: () => {
        this.dataSource = new MatTableDataSource(employeesPermissionsResponse);
        this.dataSource.sort = this.sort;
        this.dataSource.paginator = this.paginator;
      }
    });
  }

  permissionsSettings(employeeID: number) {
    this.dialog.open(PermissionsModalComponent, {
      data: {
        employeeID
      },
      panelClass: 'editarTicketModal',
      width: '500px',
      height: '550px',
      disableClose: true
    });
  }

}
