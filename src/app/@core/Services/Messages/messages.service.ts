import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { MessagesModel } from '../../Models/Messages/Messages@Model';

@Injectable({ providedIn: 'root' })
export class MessagesService {
    constructor(
        private http: HttpClient
    ) { }

    getMessagesByIdTicket(ticketID: number): Observable<MessagesModel> {
        return this.http.get<MessagesModel>(`${environment.baseURL}/chat/${ticketID}`);
    }

    postMessagesChat(_ticketID: number, _messages: string) {
        const arrayDataChat = { ticketID: _ticketID, messages: _messages };
        return this.http.post<MessagesModel>(`${environment.baseURL}/chat`, arrayDataChat);
    }

    putMessagesChat(_ticketID: number, _messages: string) {
        const arrayDataChat = { ticketID: _ticketID, messages: _messages };
        return this.http.put<MessagesModel>(`${environment.baseURL}/chat`, arrayDataChat);
    }

    uploadImage(name: string, file: File, tickeID: number): Observable<Object> {
        const form = new FormData();
        form.append('name', name);
        form.append('chat', tickeID.toString());
        form.append('file', file, 'form-data');
        return this.http.post<Object>(`${environment.baseURL}/images/upload`, form);
    }

}